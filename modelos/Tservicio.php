<?php 


Class Tservicio{
  
  protected $idtservicio;
  protected $nombre;
  
  public function __construct() {
        
  }
  public function list($idtservicio = false, $nombre = false){
    $sql = "SELECT idtservicio, nombre FROM tservicio";
    if($idtservicio){
      $sql .= " WHERE id= $idtservicio"; 
    }
    return db_query($sql);
  } 

  function tservicio($idtservicio = false, $nombre = false) {
    $query = $this->list($idtservicio, $nombre);
    if ($row = db_fetch($query)) {
      $this->setIdtservicio($row["idtservicio"]);
      $this->setNombre($row["nombre"]);  
    }
  }


  /**
   * Get the value of idtservicio
   */ 
  public function getIdtservicio()
  {
    return $this->idtservicio;
  }

  /**
   * Set the value of idtservicio
   *
   * @return  self
   */ 
  public function setIdtservicio($idtservicio)
  {
    $this->idtservicio = $idtservicio;

    return $this;
  }

  /**
   * Get the value of nombre
   */ 
  public function getNombre()
  {
    return $this->nombre;
  }

  /**
   * Set the value of nombre
   *
   * @return  self
   */ 
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;

    return $this;
  }
}