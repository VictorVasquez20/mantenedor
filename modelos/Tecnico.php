<?php 
class Tecnico {

    protected $idtecnico;
    protected $idcargotec;
    protected $idsupervisor;
    protected $nombre;
    protected $apellidos;
    protected $rut;
    protected $telefono_interno;
    protected $email_interno;
    protected $condicion;

    public function __construct() {
        
    }

    public function list($idtecnico = false,$idcargotec = false, $idsupervisor = false, $nombre = false, $apellidos = false, $rut = false, $telefono_interno = false, $email_interno = false, $condicion = '1'){
        $sql = "SELECT idtecnico, idcargotec, idsupervisor, nombre, apellidos, rut, telefono_interno, email_interno, condicion
                FROM tecnico
                WHERE condicion in($condicion)";
        if($rut){
            $sql .= " AND rut = '$rut'";
        }
        return db_query($sql);
    }
    public function insertTecnico($idcargotec, $idsupervisor, $nombre, $apellidos, $rut, $telefono_interno, $email_interno, $condicion){
        $sql = "INSERT INTO tecnico(idcargotec, idsupervisor, nombre, apellidos, rut, telefono_interno, email_interno, condicion) VALUES($idcargotec, $idsupervisor, '$nombre', '$apellidos', '$rut', '$telefono_interno', '$email_interno', $condicion)";
        return ejecutarConsulta($sql);
    }
    public function updateTecnico($idtecnico,$idcategoria, $idSupervisor,$nombre,$apellido,$num_documento,$movil,$email_corporativo){
        //$sql = "UPDATE tecnico SET idcargotec = $idcargotec, idsupervisor = $idSupervisor, nombre = '$nombre', apellidos = '$apellido', rut = '19.223.284-8', telefono_interno = '$movil', email_interno = '$email_corporativo' WHERE idtecnico = $idtecnico";
        $sql = "UPDATE tecnico SET idcargotec = $idcategoria, idsupervisor = $idSupervisor, nombre = '$nombre', apellidos = '$apellido', rut = '$num_documento', telefono_interno = '$movil', email_interno = '$email_corporativo', condicion = 1 WHERE idtecnico =$idtecnico";
        return ejecutarConsulta($sql);
    }
    function tecnico($idtecnico = false, $idcargotec = false, $idsupervisor = false, $nombre = false, $apellidos = false, $rut = false, $telefono_interno = false, $email_interno = false, $condicion = '0,1')
	{
		$query = $this->list($idtecnico, $idcargotec, $idsupervisor, $nombre, $apellidos, $rut, $telefono_interno, $email_interno, $condicion);

		if ($row = db_fetch($query)) {
            $this->setIdtecnico($row['idtecnico']);
            $this->setIdcargotec($row['idcargotec']);
            $this->setIdsupervisor($row['idsupervisor']);
            $this->setNombre($row['nombre']);
            $this->setApellidos($row['apellidos']);
            $this->setRut($row['rut']);
            $this->setTelefono_interno($row['telefono_interno']);
            $this->setEmail_interno($row['email_interno']);
            $this->setCondicion($row['condicion']);
		
		}
    }
    public function desactivar($idtecnico){
		$sql="UPDATE tecnico SET condicion='0' WHERE idtecnico=$idtecnico";
		return ejecutarConsulta($sql);
    }
    public function activar($idtecnico){
		$sql="UPDATE tecnico SET condicion='1' WHERE idtecnico=$idtecnico";
		return ejecutarConsulta($sql);
	}
    public function validarExisteNumDocumento($num_documento) {
                                        
		$sql="SELECT count(1) as cantidad, condicion FROM tecnico WHERE rut='$num_documento'";
		return ejecutarConsultaSimpleFila($sql);
    }
    public function dataTec($rut){
        $sql = "SELECT idtecnico, idcargotec, idsupervisor, nombre, apellidos, rut, telefono_interno, email_interno, condicion
        FROM tecnico
        WHERE condicion in('0') AND rut = '$rut'";
        return ejecutarConsultaSimpleFila($sql);
    }
    public function dataTecActivo($rut){
        $sql = "SELECT count(1) as cantidad, idtecnico, idcargotec, idsupervisor, nombre, apellidos, rut, telefono_interno, email_interno, condicion
        FROM tecnico
        WHERE condicion in('1') AND rut = '$rut'";
        return ejecutarConsultaSimpleFila($sql);
    }
    
    /**
     * Get the value of idtecnico
     */ 
    public function getIdtecnico()
    {
        return $this->idtecnico;
    }

    /**
     * Set the value of idtecnico
     *
     * @return  self
     */ 
    public function setIdtecnico($idtecnico)
    {
        $this->idtecnico = $idtecnico;

        return $this;
    }

    /**
     * Get the value of idcargotec
     */ 
    public function getIdcargotec()
    {
        return $this->idcargotec;
    }

    /**
     * Set the value of idcargotec
     *
     * @return  self
     */ 
    public function setIdcargotec($idcargotec)
    {
        $this->idcargotec = $idcargotec;

        return $this;
    }

    /**
     * Get the value of idsupervisor
     */ 
    public function getIdsupervisor()
    {
        return $this->idsupervisor;
    }

    /**
     * Set the value of idsupervisor
     *
     * @return  self
     */ 
    public function setIdsupervisor($idsupervisor)
    {
        $this->idsupervisor = $idsupervisor;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of apellidos
     */ 
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set the value of apellidos
     *
     * @return  self
     */ 
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get the value of rut
     */ 
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set the value of rut
     *
     * @return  self
     */ 
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get the value of telefono_interno
     */ 
    public function getTelefono_interno()
    {
        return $this->telefono_interno;
    }

    /**
     * Set the value of telefono_interno
     *
     * @return  self
     */ 
    public function setTelefono_interno($telefono_interno)
    {
        $this->telefono_interno = $telefono_interno;

        return $this;
    }

    /**
     * Get the value of email_interno
     */ 
    public function getEmail_interno()
    {
        return $this->email_interno;
    }

    /**
     * Set the value of email_interno
     *
     * @return  self
     */ 
    public function setEmail_interno($email_interno)
    {
        $this->email_interno = $email_interno;

        return $this;
    }

    /**
     * Get the value of condicion
     */ 
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set the value of condicion
     *
     * @return  self
     */ 
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;

        return $this;
    }
}