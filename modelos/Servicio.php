<?php 

Class Servicio{
  protected $idservicio;
  protected $idtservicio;
  protected $iduser;
  protected $idtecnico;
  protected $idascensor;
  protected $estadoini;
  protected $observacionini;
  protected $estadofin;
  protected $observacionfin;
  protected $nrodocumento;
  protected $nombre;
  protected $apellidos;
  protected $rut;
  protected $firma;
  protected $reqfirma;
  protected $idfactura;
  protected $idcentrocos;
  protected $facturacion;
  protected $infopro;
  protected $infodev;
  protected $estado;
  protected $created_time;
  protected $closed_time;
  protected $latini;
  protected $lonini;
  protected $latfin;
  protected $lonfin;
  protected $file;
  protected $filefir;
  protected $condicion;


  public function __construct() {
        
  }

  public function list($idservicio = false,  $idtservicio = false,  $iduser = false,  $idtecnico = false,  $idascensor = false,  $estadoini = false,  $observacionini = false,  $estadofin = false,  $observacionfin = false,  $nrodocumento = false,  $nombre = false,  $apellidos = false,  $rut = false,  $firma = false,  $reqfirma = false,  $idfactura = false,  $idcentrocos = false,  $facturacion = false,  $infopro = false,  $infodev = false,  $estado = false,  $created_time = false,  $closed_time = false,  $latini = false,  $lonini = false,  $latfin = false,  $lonfin = false,  $file = false,  $filefir = false,  $condicion = '0,1'){
    $sql = "SELECT idservicio, idtservicio, iduser, idtecnico, idascensor, estadoini, observacionini, estadofin, observacionfin, nrodocumento, nombre, apellidos, rut, firma, reqfirma, idfactura, idcentrocos, facturacion, infopro, infodev, estado, created_time, closed_time, latini, lonini, latfin, lonfin, file, filefir, condicion
    FROM servicio
    WHERE condicion in($condicion)";
    if($idservicio){
      $sql .= " AND idservicio = $idservicio";
    }
    return db_query($sql);      
  }
   
  function servicio($idservicio = false,  $idtservicio = false,  $iduser = false,  $idtecnico = false,  $idascensor = false,  $estadoini = false,  $observacionini = false,  $estadofin = false,  $observacionfin = false,  $nrodocumento = false,  $nombre = false,  $apellidos = false,  $rut = false,  $firma = false,  $reqfirma = false,  $idfactura = false,  $idcentrocos = false,  $facturacion = false,  $infopro = false,  $infodev = false,  $estado = false,  $created_time = false,  $closed_time = false,  $latini = false,  $lonini = false,  $latfin = false,  $lonfin = false,  $file = false,  $filefir = false,  $condicion = '0,1') {
    $query = $this->list($idservicio,  $idtservicio,  $iduser,  $idtecnico,  $idascensor,  $estadoini,  $observacionini,  $estadofin,  $observacionfin,  $nrodocumento,  $nombre,  $apellidos,  $rut,  $firma,  $reqfirma,  $idfactura,  $idcentrocos,  $facturacion,  $infopro,  $infodev,  $estado,  $created_time,  $closed_time,  $latini,  $lonini,  $latfin,  $lonfin,  $file,  $filefir,  $condicion);
    
    if ($row = db_fetch($query)) {
        $this->setIdservicio($row["idservicio"]);
        $this->setIdtservicio($row["idtservicio"]);
        $this->setIduser($row["iduser"]);
        $this->setIdtecnico($row["idtecnico"]);
        $this->setIdascensor($row["idascensor"]);
        $this->setObservacionini($row["observacionini"]);
        $this->setEstadoini($row["estadoini"]);
        $this->setEstadofin($row["estadofin"]);
        $this->setObservacionfin($row["observacionfin"]);
        $this->setNrodocumento($row["nrodocumento"]);
        $this->setNombre($row["nombre"]);
        $this->setApellidos($row["apellidos"]);
        $this->setRut($row["rut"]);
        $this->setFirma($row["firma"]);
        $this->setReqfirma($row["reqfirma"]);
        $this->setIdfactura($row["idfactura"]);
        $this->setIdcentrocos($row["idcentrocos"]);
        $this->setFacturacion($row["facturacion"]);
        $this->setInfopro($row["infopro"]);
        $this->setInfodev($row["infodev"]);
        $this->setEstado($row["estado"]);
        $this->setCreated_time($row["created_time"]);
        $this->setClosed_time($row["closed_time"]);
        $this->setLatini($row["latini"]);
        $this->setLonini($row["lonini"]);
        $this->setLatfin($row["latfin"]);
        $this->setLonfin($row["lonfin"]);
        $this->setFile($row["file"]);
        $this->setFilefir($row["filefir"]);
        $this->setCondicion($row["condicion"]);
    }
  }
  public function listDataTable($fechadesde, $fechaHasta, $guia){
    $sql = "SELECT s.idservicio, ed.nombre AS nombreEdi, ascs.codigo, tec.nombre AS nombreTec, tec.apellidos AS apellidoTec,CAST( s.created_time AS DATE)
            FROM servicio s
            JOIN tservicio ts ON s.idtservicio = ts.idtservicio
            JOIN tecnico tec ON s.idtecnico = tec.idtecnico
            JOIN ascensor ascs ON s.idascensor = ascs.idascensor
            JOIN edificio ed ON ascs.idedificio = ed.idedificio
            WHERE CAST( s.created_time AS DATE) >= '$fechadesde' 
            AND CAST( s.created_time AS DATE) < '$fechaHasta' 
            AND (s.idservicio = $guia OR $guia = 0) 
            ORDER BY CAST( s.created_time AS DATE)  "; //Cambiar fecha fija por fecha hasta
   
     return db_query($sql);
  }

  public function update($idservicio, $idascensor, $idtservicio, $estadoini, $estadofin, $observacionini, $observacionfin) {
		$sql = "UPDATE servicio SET idtservicio = $idtservicio, idascensor = $idascensor, estadoini = $estadoini, observacionini = '$observacionini', estadofin = $estadofin, observacionfin = '$observacionfin' WHERE idservicio = $idservicio ";
    return ejecutarConsulta($sql);
  }


  /**
   * Get the value of idservicio
   */ 
  public function getIdservicio()
  {
    return $this->idservicio;
  }

  /**
   * Set the value of idservicio
   *
   * @return  self
   */ 
  public function setIdservicio($idservicio)
  {
    $this->idservicio = $idservicio;

    return $this;
  }

  /**
   * Get the value of idtservicio
   */ 
  public function getIdtservicio()
  {
    return $this->idtservicio;
  }

  /**
   * Set the value of idtservicio
   *
   * @return  self
   */ 
  public function setIdtservicio($idtservicio)
  {
    $this->idtservicio = $idtservicio;

    return $this;
  }

  /**
   * Get the value of iduser
   */ 
  public function getIduser()
  {
    return $this->iduser;
  }

  /**
   * Set the value of iduser
   *
   * @return  self
   */ 
  public function setIduser($iduser)
  {
    $this->iduser = $iduser;

    return $this;
  }

  /**
   * Get the value of idtecnico
   */ 
  public function getIdtecnico()
  {
    return $this->idtecnico;
  }

  /**
   * Set the value of idtecnico
   *
   * @return  self
   */ 
  public function setIdtecnico($idtecnico)
  {
    $this->idtecnico = $idtecnico;

    return $this;
  }

  /**
   * Get the value of idascensor
   */ 
  public function getIdascensor()
  {
    return $this->idascensor;
  }

  /**
   * Set the value of idascensor
   *
   * @return  self
   */ 
  public function setIdascensor($idascensor)
  {
    $this->idascensor = $idascensor;

    return $this;
  }

  /**
   * Get the value of observacionini
   */ 
  public function getObservacionini()
  {
    return $this->observacionini;
  }

  /**
   * Set the value of observacionini
   *
   * @return  self
   */ 
  public function setObservacionini($observacionini)
  {
    $this->observacionini = $observacionini;

    return $this;
  }

  /**
   * Get the value of estadoini
   */ 
  public function getEstadoini()
  {
    return $this->estadoini;
  }

  /**
   * Set the value of estadoini
   *
   * @return  self
   */ 
  public function setEstadoini($estadoini)
  {
    $this->estadoini = $estadoini;

    return $this;
  }

  /**
   * Get the value of estadofin
   */ 
  public function getEstadofin()
  {
    return $this->estadofin;
  }

  /**
   * Set the value of estadofin
   *
   * @return  self
   */ 
  public function setEstadofin($estadofin)
  {
    $this->estadofin = $estadofin;

    return $this;
  }

  /**
   * Get the value of observacionfin
   */ 
  public function getObservacionfin()
  {
    return $this->observacionfin;
  }

  /**
   * Set the value of observacionfin
   *
   * @return  self
   */ 
  public function setObservacionfin($observacionfin)
  {
    $this->observacionfin = $observacionfin;

    return $this;
  }

  /**
   * Get the value of nrodocumento
   */ 
  public function getNrodocumento()
  {
    return $this->nrodocumento;
  }

  /**
   * Set the value of nrodocumento
   *
   * @return  self
   */ 
  public function setNrodocumento($nrodocumento)
  {
    $this->nrodocumento = $nrodocumento;

    return $this;
  }

  /**
   * Get the value of nombre
   */ 
  public function getNombre()
  {
    return $this->nombre;
  }

  /**
   * Set the value of nombre
   *
   * @return  self
   */ 
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;

    return $this;
  }

  /**
   * Get the value of apellidos
   */ 
  public function getApellidos()
  {
    return $this->apellidos;
  }

  /**
   * Set the value of apellidos
   *
   * @return  self
   */ 
  public function setApellidos($apellidos)
  {
    $this->apellidos = $apellidos;

    return $this;
  }

  /**
   * Get the value of rut
   */ 
  public function getRut()
  {
    return $this->rut;
  }

  /**
   * Set the value of rut
   *
   * @return  self
   */ 
  public function setRut($rut)
  {
    $this->rut = $rut;

    return $this;
  }

  /**
   * Get the value of firma
   */ 
  public function getFirma()
  {
    return $this->firma;
  }

  /**
   * Set the value of firma
   *
   * @return  self
   */ 
  public function setFirma($firma)
  {
    $this->firma = $firma;

    return $this;
  }

  /**
   * Get the value of reqfirma
   */ 
  public function getReqfirma()
  {
    return $this->reqfirma;
  }

  /**
   * Set the value of reqfirma
   *
   * @return  self
   */ 
  public function setReqfirma($reqfirma)
  {
    $this->reqfirma = $reqfirma;

    return $this;
  }

  /**
   * Get the value of idfactura
   */ 
  public function getIdfactura()
  {
    return $this->idfactura;
  }

  /**
   * Set the value of idfactura
   *
   * @return  self
   */ 
  public function setIdfactura($idfactura)
  {
    $this->idfactura = $idfactura;

    return $this;
  }

  /**
   * Get the value of idcentrocos
   */ 
  public function getIdcentrocos()
  {
    return $this->idcentrocos;
  }

  /**
   * Set the value of idcentrocos
   *
   * @return  self
   */ 
  public function setIdcentrocos($idcentrocos)
  {
    $this->idcentrocos = $idcentrocos;

    return $this;
  }

  /**
   * Get the value of facturacion
   */ 
  public function getFacturacion()
  {
    return $this->facturacion;
  }

  /**
   * Set the value of facturacion
   *
   * @return  self
   */ 
  public function setFacturacion($facturacion)
  {
    $this->facturacion = $facturacion;

    return $this;
  }

  /**
   * Get the value of infopro
   */ 
  public function getInfopro()
  {
    return $this->infopro;
  }

  /**
   * Set the value of infopro
   *
   * @return  self
   */ 
  public function setInfopro($infopro)
  {
    $this->infopro = $infopro;

    return $this;
  }

  /**
   * Get the value of infodev
   */ 
  public function getInfodev()
  {
    return $this->infodev;
  }

  /**
   * Set the value of infodev
   *
   * @return  self
   */ 
  public function setInfodev($infodev)
  {
    $this->infodev = $infodev;

    return $this;
  }

  /**
   * Get the value of estado
   */ 
  public function getEstado()
  {
    return $this->estado;
  }

  /**
   * Set the value of estado
   *
   * @return  self
   */ 
  public function setEstado($estado)
  {
    $this->estado = $estado;

    return $this;
  }

  /**
   * Get the value of created_time
   */ 
  public function getCreated_time()
  {
    return $this->created_time;
  }

  /**
   * Set the value of created_time
   *
   * @return  self
   */ 
  public function setCreated_time($created_time)
  {
    $this->created_time = $created_time;

    return $this;
  }

  /**
   * Get the value of closed_time
   */ 
  public function getClosed_time()
  {
    return $this->closed_time;
  }

  /**
   * Set the value of closed_time
   *
   * @return  self
   */ 
  public function setClosed_time($closed_time)
  {
    $this->closed_time = $closed_time;

    return $this;
  }

  /**
   * Get the value of latini
   */ 
  public function getLatini()
  {
    return $this->latini;
  }

  /**
   * Set the value of latini
   *
   * @return  self
   */ 
  public function setLatini($latini)
  {
    $this->latini = $latini;

    return $this;
  }

  /**
   * Get the value of lonini
   */ 
  public function getLonini()
  {
    return $this->lonini;
  }

  /**
   * Set the value of lonini
   *
   * @return  self
   */ 
  public function setLonini($lonini)
  {
    $this->lonini = $lonini;

    return $this;
  }

  /**
   * Get the value of latfin
   */ 
  public function getLatfin()
  {
    return $this->latfin;
  }

  /**
   * Set the value of latfin
   *
   * @return  self
   */ 
  public function setLatfin($latfin)
  {
    $this->latfin = $latfin;

    return $this;
  }

  /**
   * Get the value of lonfin
   */ 
  public function getLonfin()
  {
    return $this->lonfin;
  }

  /**
   * Set the value of lonfin
   *
   * @return  self
   */ 
  public function setLonfin($lonfin)
  {
    $this->lonfin = $lonfin;

    return $this;
  }

  /**
   * Get the value of file
   */ 
  public function getFile()
  {
    return $this->file;
  }

  /**
   * Set the value of file
   *
   * @return  self
   */ 
  public function setFile($file)
  {
    $this->file = $file;

    return $this;
  }

  /**
   * Get the value of filefir
   */ 
  public function getFilefir()
  {
    return $this->filefir;
  }

  /**
   * Set the value of filefir
   *
   * @return  self
   */ 
  public function setFilefir($filefir)
  {
    $this->filefir = $filefir;

    return $this;
  }

  /**
   * Get the value of condicion
   */ 
  public function getCondicion()
  {
    return $this->condicion;
  }

  /**
   * Set the value of condicion
   *
   * @return  self
   */ 
  public function setCondicion($condicion)
  {
    $this->condicion = $condicion;

    return $this;
  }
}