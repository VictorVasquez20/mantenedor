<?php 

Class Role{
   
  protected $idrole;
  protected $nombre;
  protected $descripcion;
  protected $condicion;
  protected $publico;
  protected $created_time;
  protected $updated_time;

  public function __construct() {
        
  }


  public function list(){
    $sql = "SELECT idrole, nombre, descripcion, condicion, publico, created_time, updated_time FROM role order by  nombre";
    return db_query($sql);
  }

  public function getIdrole()
  {
        return $this->idrole;
  }
  public function setIdrole($idrole)
  {
        $this->idrole = $idrole;

        return $this;
  }

  public function getNombre()
  {
        return $this->nombre;
  }
  public function setNombre($nombre)
  {
        $this->nombre = $nombre;

        return $this;
  }

  public function getDescripcion()
  {
        return $this->descripcion;
  }
  public function setDescripcion($descripcion)
  {
        $this->descripcion = $descripcion;

        return $this;
  }

  public function getCondicion()
  {
        return $this->condicion;
  }
  public function setCondicion($condicion)
  {
        $this->condicion = $condicion;

        return $this;
  }

  public function getPublico()
  {
        return $this->publico;
  }
  public function setPublico($publico)
  {
        $this->publico = $publico;

        return $this;
  }

  public function getCreated_time()
  {
        return $this->created_time;
  }
  public function setCreated_time($created_time)
  {
        $this->created_time = $created_time;

        return $this;
  }

  public function getUpdated_time()
  {
        return $this->updated_time;
  }
  public function setUpdated_time($updated_time)
  {
        $this->updated_time = $updated_time;

        return $this;
  }
}