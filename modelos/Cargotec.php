<?php 

class Cargotec {
    protected $idcargotec;
    protected $nombre;
    protected $condicion;
    public function __construct() {
        
    }
    public function list(){
        $sql = "SELECT idcargotec, nombre, condicion FROM cargotec order by nombre";
        return db_query($sql);
    }


    /**
     * Get the value of idcargotec
     */ 
    public function getIdcargotec()
    {
        return $this->idcargotec;
    }

    /**
     * Set the value of idcargotec
     *
     * @return  self
     */ 
    public function setIdcargotec($idcargotec)
    {
        $this->idcargotec = $idcargotec;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of condicion
     */ 
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set the value of condicion
     *
     * @return  self
     */ 
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;

        return $this;
    }
}