<?php

class Edificio {
  protected $idedificio;
  protected $nombre;
  protected $calle;
  protected $numero;
  protected $oficina;
  protected $coordinacion;
  protected $residente;
  protected $condicion;
  protected $created_time;
  protected $updated_time;
  protected $idtsegmento;
  protected $idregiones;
  protected $idprovincias;
  protected $idcomunas;

  public function __construct() {
        
  }

  public function list($idedificio = false){
    $sql = "SELECT idedificio, nombre, calle, numero, oficina, coordinacion, residente, condicion, created_time, updated_time, idtsegmento, idregiones, idprovincias, idcomunas FROM edificio";
    if($idedificio){
      $sql .= " WHERE idedificio = $idedificio";
    }
    return db_query($sql);
  }

  public function listLike($buscar){
      $sql = "SELECT idedificio, nombre FROM edificio WHERE nombre like '%$buscar%'";
      return db_query($sql);
  }
  public function edificioList($idedificio = false){
    $query = $this->list($idedificio);
    if ($row = db_fetch($query)) {
      $this->setIdedificio($row['idedificio']);
      $this->setNombre($row['nombre']);
      $this->setCalle($row['calle']);
      $this->setNumero($row['numero']);
      $this->setOficina($row['oficina']);
      $this->setCoordinacion($row['coordinacion']);
      $this->setResidente($row['residente']);
      $this->setCondicion($row['condicion']);
      $this->setCreated_time($row['created_time']);
      $this->setUpdated_time($row['updated_time']);
      $this->setIdtsegmento($row['idtsegmento']);
      $this->setIdregiones($row['idregiones']);
      $this->setIdprovincias($row['idprovincias']);
      $this->setIdcomunas($row['idcomunas']);
    }
  }
  public function getIdedificio()
  {
    return $this->idedificio;
  }
  public function setIdedificio($idedificio)
  {
    $this->idedificio = $idedificio;

    return $this;
  }

  public function getNombre()
  {
    return $this->nombre;
  }
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;

    return $this;
  }

  public function getCalle()
  {
    return $this->calle;
  }
  public function setCalle($calle)
  {
    $this->calle = $calle;

    return $this;
  }
 
  public function getOficina()
  {
    return $this->oficina;
  }
  public function setOficina($oficina)
  {
    $this->oficina = $oficina;

    return $this;
  }

  public function getNumero()
  {
    return $this->numero;
  }
  public function setNumero($numero)
  {
    $this->numero = $numero;

    return $this;
  }

  public function getCoordinacion()
  {
    return $this->coordinacion;
  }
  public function setCoordinacion($coordinacion)
  {
    $this->coordinacion = $coordinacion;

    return $this;
  }

  public function getResidente()
  {
    return $this->residente;
  }
  public function setResidente($residente)
  {
    $this->residente = $residente;

    return $this;
  }


  public function getCondicion()
  {
    return $this->condicion;
  }
  public function setCondicion($condicion)
  {
    $this->condicion = $condicion;

    return $this;
  }

  public function getCreated_time()
  {
    return $this->created_time;
  }
  public function setCreated_time($created_time)
  {
    $this->created_time = $created_time;

    return $this;
  }

  public function getUpdated_time()
  {
    return $this->updated_time;
  }
  public function setUpdated_time($updated_time)
  {
    $this->updated_time = $updated_time;

    return $this;
  }

  
  public function getIdtsegmento()
  {
    return $this->idtsegmento;
  }
  public function setIdtsegmento($idtsegmento)
  {
    $this->idtsegmento = $idtsegmento;

    return $this;
  }


  public function getIdregiones()
  {
    return $this->idregiones;
  }
  public function setIdregiones($idregiones)
  {
    $this->idregiones = $idregiones;

    return $this;
  }

 
  public function getIdprovincias()
  {
    return $this->idprovincias;
  }
  public function setIdprovincias($idprovincias)
  {
    $this->idprovincias = $idprovincias;

    return $this;
  }


  public function getIdcomunas()
  {
    return $this->idcomunas;
  }

  public function setIdcomunas($idcomunas)
  {
    $this->idcomunas = $idcomunas;

    return $this;
  }
}