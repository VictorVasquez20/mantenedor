<?php 


Class Testado{
  
  protected $id;
  protected $nombre;
  protected $fin;

  public function __construct() {
        
  }

  public function list($id = false, $nombre = false, $fin = false){
    $sql = "SELECT id, nombre, fin FROM testado";
    if($id){
      $sql .= " WHERE id= $id"; 
    }
    return db_query($sql);
  } 

  /**
   * Get the value of id
   */ 
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set the value of id
   *
   * @return  self
   */ 
  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  /**
   * Get the value of nombre
   */ 
  public function getNombre()
  {
    return $this->nombre;
  }

  /**
   * Set the value of nombre
   *
   * @return  self
   */ 
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;

    return $this;
  }

  /**
   * Get the value of fin
   */ 
  public function getFin()
  {
    return $this->fin;
  }

  /**
   * Set the value of fin
   *
   * @return  self
   */ 
  public function setFin($fin)
  {
    $this->fin = $fin;

    return $this;
  }
 
}