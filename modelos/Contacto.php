<?php 

class Contacto {
  protected $idcontacto;
  protected $nombre;
  protected $cargo;
  protected $email;
  protected $telefonomovil;
  protected $telefonoresi;
  protected $idedificio;

  public function __construct() {
        
}

  public function list($idcontacto = false){
	$sql = "SELECT idcontacto, nombre, cargo, email, telefonomovil, telefonoresi, idedificio FROM contacto";
	if($idcontacto){
		$sql .= " WHERE idcontacto = $idcontacto";
	}
    return db_query($sql);
  }
  public function listEdificio(){
    $sql = "SELECT c.idcontacto, c.nombre, c.cargo, c.email, c.telefonomovil, c.telefonoresi, e.nombre as nombreEdificio 
            FROM contacto c
            JOIN edificio e on c.idedificio = e.idedificio
            ORDER BY e.nombre";
    return db_query($sql);
  }

  public function insertMany($nombre, $cargo, $email, $telefonomovil, $telefonoresi, $idedificio){
	$sql = "INSERT INTO contacto(nombre, cargo, email, telefonomovil, telefonoresi, idedificio) VALUES('$nombre', '$cargo', '$email', '$telefonomovil', '$telefonoresi', $idedificio)";
    return ejecutarConsulta($sql);
  }
  public function contactos($idcontacto = false){
	$query = $this->list($idcontacto);
	if ($row = db_fetch($query)) {
		$this->set_idcontacto($row['idcontacto']);
		$this->set_nombre($row['nombre']);
		$this->set_cargo($row['cargo']);
		$this->set_email($row['email']);
		$this->set_telefonomovil($row['telefonomovil']);
		$this->set_telefonoresi($row['telefonoresi']);
		$this->set_idedificio($row['idedificio']);
	}
  }
  public function deleteContacto($idcontacto){
	$sql = "DELETE FROM contacto WHERE idcontacto = $idcontacto";
	return ejecutarConsulta($sql);
  }
  public function update($idcontacto,$nombre, $cargo, $email, $telefonomovil, $telefonoresi){	
	$sql = "UPDATE contacto set nombre = '$nombre', cargo = '$cargo', email = '$email', telefonomovil = '$telefonomovil', telefonoresi = '$telefonoresi' where idcontacto = $idcontacto";
    return ejecutarConsulta($sql);
  }

  public function validarCorreo($email, $idEdificio){
	$sql = "SELECT COUNT(1) as cantidad from contacto where email = '$email' and idedificio = $idEdificio";
	return ejecutarConsultaSimpleFila($sql);

  }

  public function get_idcontacto()
	{
		return $this->idcontacto;
	}

	public function set_idcontacto($idcontacto)
	{
		$this->idcontacto = $idcontacto;
  }
  
  public function get_nombre()
	{
		return $this->nombre;
	}

	public function set_nombre($nombre)
	{
		$this->nombre = $nombre;
	}

  public function get_cargo()
	{
		return $this->cargo;
	}

	public function set_cargo($cargo)
	{
		$this->cargo = $cargo;
  }
  
  public function get_email()
	{
		return $this->email;
	}

	public function set_email($email)
	{
		$this->email = $email;
  }
  
  public function get_telefonomovil()
	{
		return $this->telefonomovil;
	}

	public function set_telefonomovil($telefonomovil)
	{
		$this->telefonomovil = $telefonomovil;
  }
  
  public function get_telefonoresi()
	{
		return $this->telefonoresi;
	}

	public function set_telefonoresi($telefonoresi)
	{
		$this->telefonoresi = $telefonoresi;
  }
  
  public function get_idedificio()
	{
		return $this->idedificio;
	}

	public function set_idedificio($idedificio)
	{
		$this->idedificio = $idedificio;
	}

  
}