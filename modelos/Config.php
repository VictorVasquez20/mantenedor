<?php
require_once "../config/global.php";
class Config
{
    public function conection(){
        $conexion = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        mysqli_query($conexion, 'SET NAMES "'.DB_ENCODE.'"');
        mysqli_query($conexion, 'SET lc_time_names="'.DB_NAMES.'"');
        if(mysqli_connect_errno()){
            printf("Fallo la conexion con la BD: %s \n", mysqli_connect_error());
            exit();
        }
        return $conexion;
    }
}