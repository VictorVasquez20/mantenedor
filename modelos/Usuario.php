<?php

require_once "../config/conexionSap.php";

class Usuario
{
	protected $iduser ;
	protected $idrole ;
	protected $username;
	protected $password ;
	protected $nombre ;
	protected $apellido;
	protected $tipo_documento;
	protected $num_documento ;
	protected $fecha_nac ;
	protected $direccion ;
	protected $telefono ;
	protected $email ;
	protected $imagen ;
	protected $ultimo_inicio ;
	protected $condicion ;
	protected $estado;
	protected $created_time ;
	protected $update_time;

	public function list1($iduser = false, $username = false, $password = false, $idrole = false,  $num_documento = false, $estado = '1, 0')
	{
		$sql = "SELECT iduser, idrole, username, password ,nombre ,apellido,tipo_documento,num_documento ,fecha_nac ,direccion ,telefono ,email ,imagen ,ultimo_inicio ,condicion ,estado,created_time ,update_time
				FROM user 
				WHERE estado IN ($estado)";

		if ($iduser) {
			$sql .= " AND iduser = $iduser";
		}

		if ($username) {
			$sql .= " AND username = '$username'";
		}

		if ($password) {
			$password = hash('SHA256', $password);
			$sql .= " AND password = '$password'";
		}

		if ($idrole) {
			$sql .= " AND idrole = $idrole";
		}

		if ($num_documento) {
			$sql .= " AND num_documento = '$num_documento'";
		}

		return db_query($sql);
		
	}

	public function list($iduser = false, $username = false, $password = false)
	{
		$sql = "SELECT iduser, idrole, username, password ,nombre ,apellido,tipo_documento,num_documento ,fecha_nac ,direccion ,telefono ,email ,imagen ,ultimo_inicio ,condicion ,estado,created_time ,update_time
				FROM user 
				WHERE condicion = 1";
		if ($iduser) {
			$sql .= " AND iduser = $iduser";
		}

		if ($username) {
			$sql .= " AND username = '$username'";
		}

		if ($password) {
			$password = hash('SHA256', $password);
			$sql .= " AND password = '$password'";
		}

		return ejecutarConsulta($sql);
		
	}

	// insert sap
	public function insertaEmpleadoSap($data){
		$data = json_decode($data);
		$datos = json_encode(
			array(
				"FirstName" => $data->nombre,
				"LastName" => $data->apellido,
				"PassportNumber" => $data->num_documento,
				"Branch" => "-2",
				"Department" => "-2",
				"Remarks" => "Creado desde Integración",
				"CitizenshipCountryCode" => "CL",
				"EmployeeRolesInfoLines" => array(
					array(
						"LineNum" => 1,
						"RoleID" => "-2"
					)
				)
			)
		);
		$entity = 'EmployeesInfo';
		$rsptaempl = InsertarDatos($entity,$datos);
		$datosEmpleado = json_decode($rsptaempl);
		return true;
	}
	//fin insert sap

	// consulta empleado sap
	public function consultaEmpleadoSap($rut){
		$entity = 'EmployeesInfo';
		$select = 'EmployeeID';
		$filter = "PassportNumber eq '".$rut."'";
		return json_decode(ConsultaEntity($entity,$select,$filter), true);
	}
	//fin consulta empleado sap

	// update empleado sap
	public function updateEmpleadoSap($id,$data){
		$data = json_decode($data);
		$datos = json_encode(
			array(
				"FirstName" => $data->nombre,
				"LastName" => $data->apellido,
				"PassportNumber" => $data->num_documento,
			)
		);
		$entity = 'EmployeesInfo';
		$rsptaempl = EditardatosNum($entity,$id,$datos);
		$datosEmpleado = json_decode($rsptaempl);
		return true;
	}
	//fin update empleado sap


	private function insert($rol,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password,$tipo_documento,$num_documento,$condicion,$estado){
		$password = hash('SHA256', $password);
		$sql = "INSERT INTO user(idrole,username,password,nombre,apellido,tipo_documento,num_documento,fecha_nac,direccion,telefono,email,imagen,condicion,estado) VALUES($rol,'$username','$password','$nombre','$apellido','$tipo_documento','$num_documento','$fecha_nac','$direccion','$movil','$email_corporativo','noimg.jpg',$condicion,$estado)";

		return ejecutarConsulta($sql);
	}
	function user($iduser = false, $username = false, $password = false)
	{
		$query = $this->list($iduser, $username, $password, $idrole, $num_documento, $estado);
		if ($row = db_fetch($query)) {
			$this->set_iduser($row['iduser']);
			$this->set_idrole($row['idrole']);
			$this->set_username($row['username']);
			$this->set_password($row['password']);
			$this->set_nombre($row['nombre']);
			$this->set_apellido($row['apellido']);
			$this->set_tipo_document($row['tipo_documento']);
			$this->set_num_documento($row['num_documento']);
			$this->set_fec_nac($row['fecha_nac']);
			$this->set_direccion($row['direccion']);
			$this->set_telefono($row['telefono']);
			$this->set_email($row['email']);
			$this->set_imagen($row['imagen']);
			$this->set_ultimo_inicio($row['ultimo_inicio']);
			$this->set_condicion($row['condicion']);
			$this->set_estado($row['estado']);
			$this->set_created_time($row['created_time']);
			$this->set_update_time($row['update_time']);
		}
	}
	public function getList(){
		return $this->list1(false,false,false,false,false,1);
	} 
	public function insertUser($rol,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password,$tipo_documento,$num_documento,$condicion,$estado){
		return $this->insert($rol,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password,$tipo_documento,$num_documento,$condicion,$estado);
	}
	public function validarExisteNumDocumento($num_documento) {
                                        
		$sql="SELECT count(1) as cantidad FROM `user` WHERE num_documento='$num_documento'";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function dataEmployees($num_documento) {
                                        
		$sql="SELECT iduser,idrole,username,password,nombre,apellido,tipo_documento,num_documento,fecha_nac,direccion,telefono,email,
				imagen,ultimo_inicio,condicion,estado,created_time,update_time 
				from user WHERE num_documento='$num_documento'";
		return ejecutarConsultaSimpleFila($sql);
	}
	private function update($iduser,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password,$tipo_documento,$num_documento, $estado, $idrole){
		$sqlpass = "SELECT password from user WHERE iduser = $iduser";
		$resp = ejecutarConsultaSimpleFila($sqlpass);
		 if ($password == $resp['password']){
			$sql = "UPDATE user SET idrole = $idrole, username = '$username', password = '$password', nombre = '$nombre', apellido = '$apellido', tipo_documento = '$tipo_documento', num_documento = '$num_documento', fecha_nac = '$fecha_nac', direccion = '$direccion', telefono = '$movil', email = '$email_corporativo', estado = $estado WHERE iduser = $iduser";
		 }else{
			$password = hash('SHA256', $password);
			$sql = "UPDATE user SET idrole = $idrole, username = '$username', password = '$password', nombre = '$nombre', apellido = '$apellido', tipo_documento = '$tipo_documento', num_documento = '$num_documento', fecha_nac = '$fecha_nac', direccion = '$direccion', telefono = '$movil', email = '$email_corporativo', estado = $estado WHERE iduser = $iduser";
		 }
		return ejecutarConsulta($sql);
		
	}
	public function updateUser($iduser,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password,$tipo_documento,$num_documento,$idrole){
		return $this->update($iduser,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password,$tipo_documento,$num_documento,1,$idrole);
	}
	public function desactivar($iduser){
		$sql="UPDATE user SET estado='0' WHERE iduser=$iduser";
		return ejecutarConsulta($sql);
	}
	public function listUsename($username){
		$sql="SELECT count(1) as cantidad, iduser FROM user WHERE username='$username'";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function buscarEmail($username){
		$sql = "SELECT email FROM user where username='$username'";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function updatePass($username,$password){
		$newpassword = hash('SHA256', $password);
		$sql = "UPDATE user SET password = '$newpassword' WHERE username='$username'";
		return ejecutarConsulta($sql);
	}
	public function get_iduser()
	{
		return $this->iduser;
	}

	function set_iduser($iduser)
	{
		$this->iduser = $iduser;
	}

	function get_idrole()
	{
		return $this->idrole;
	}

	function set_idrole($idrole)
	{
		$this->idrole = $idrole;
	}

	function get_username()
	{
		return $this->username;
	}

	function set_username($username)
	{
		$this->username = $username;
	}

	function get_password()
	{
		return $this->password;
	}

	function set_password($password)
	{
		$this->password = $password;
	}

	function get_nombre()
	{
		return $this->nombre;
	}

	function set_nombre($nombre)
	{
		$this->nombre = $nombre;
	}

	function get_apellido()
	{
		return $this->apellido;
	}

	function set_apellido($apellido)
	{
		$this->apellido = $apellido;
	}

	function get_tipo_document()
	{
		return $this->tipo_document;
	}

	function set_tipo_document($tipo_document)
	{
		$this->tipo_document = $tipo_document;
	}

	function get_num_documento()
	{
		return $this->num_documento;
	}

	function set_num_documento($num_documento)
	{
		$this->num_documento = $num_documento;
	}

	function get_fec_nac()
	{
		return $this->fec_nac;
	}

	function set_fec_nac($fecha_nac)
	{
		$this->fec_nac = $fecha_nac;
	}

	function get_direccion()
	{
		return $this->direccion;
	}

	function set_direccion($direccion)
	{
		$this->direccion = $direccion;
	}

	function get_telefono()
	{
		return $this->telefono;
	}

	function set_telefono($telefono)
	{
		$this->telefono = $telefono;
	}

	function get_email()
	{
		return $this->email;
	}

	function set_email($email)
	{
		$this->email = $email;
	}

	function get_imagen()
	{
		return $this->imagen;
	}

	function set_imagen($imagen)
	{
		$this->imagen = $imagen;
	}

	function get_ultimo_inicio()
	{
		return $this->ultimo_inicio;
	}

	function set_ultimo_inicio($ultimo_inicio)
	{
		$this->ultimo_inicio = $ultimo_inicio;
	}

	function get_condicion()
	{
		return $this->condicion;
	}

	function set_condicion($condicion)
	{
		$this->condicion = $condicion;
	}

	function get_estado()
	{
		return $this->estado;
	}

	function set_estado($estado)
	{
		$this->estado = $estado;
	}

	function get_created_time()
	{
		return $this->created_time;
	}

	function set_created_time($created_time)
	{
		$this->created_time = $created_time;
	}

	function get_update_time()
	{
		return $this->update_time;
	}

	function set_update_time($update_time)
	{
		$this->update_time = $update_time;
	}


}