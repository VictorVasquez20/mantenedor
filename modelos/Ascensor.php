<?php

Class Ascensor{

  protected $idascensor;
  protected $iduser;
  protected $idedificio;
  protected $idcontrato;
  protected $idventa;
  protected $idtascensor;
  protected $idcentrocosto;
  protected $marca;
  protected $modelo;
  protected $ubicacion;
  protected $codigo;
  protected $codigocli;
  protected $ken;
  protected $valoruf;
  protected $valorclp;
  protected $comando;
  protected $paradas;
  protected $accesos;
  protected $capper;
  protected $capkg;
  protected $velocidad;
  protected $dcs;
  protected $elink;
  protected $pservicio;
  protected $gtecnica;
  protected $estadoins;
  protected $montosi;
  protected $montosn;
  protected $estado;
  protected $monitoreo;
  protected $lat;
  protected $lon;
  protected $iddirk;
  protected $condicion;
  protected $created_time;
  protected $updated_time;
  protected $created_user;
  protected $updated_user;
  protected $closed_time;
  public function __construct() {
        
  }
  public function list($idascensor= false, $iduser= false, $idedificio= false, $idcontrato= false, $idventa= false, $idtascensor= false, $idcentrocosto= false, $marca= false, $modelo= false, $ubicacion= false, $codigo= false, $codigocli= false, $ken= false, $valoruf= false, $valorclp= false, $comando= false, $paradas= false, $accesos= false, $capper= false, $capkg= false, $velocidad= false, $dcs= false, $elink= false, $pservicio= false, $gtecnica= false, $estadoins= false, $montosi= false, $montosn= false, $estado= false, $monitoreo= false, $lat= false, $lon= false, $iddirk= false, $condicion= '0,1', $created_time= false, $updated_time= false, $created_user= false, $updated_user= false, $closed_time= false ) {
    $sql = "SELECT idascensor, iduser, idedificio, idcontrato, idventa, idtascensor, idcentrocosto, marca, modelo, ubicacion, codigo, codigocli, ken, valoruf, valorclp, comando, paradas, accesos, capper, capkg, velocidad, dcs, elink, pservicio, gtecnica, estadoins, montosi, montosn, estado, monitoreo, lat, lon, iddirk, condicion, created_time, updated_time, created_user, updated_user, closed_time
            FROM ascensor";
    return db_query($sql);
  }

  function ascensor($idascensor= false, $iduser= false, $idedificio= false, $idcontrato= false, $idventa= false, $idtascensor= false, $idcentrocosto= false, $marca= false, $modelo= false, $ubicacion= false, $codigo= false, $codigocli= false, $ken= false, $valoruf= false, $valorclp= false, $comando= false, $paradas= false, $accesos= false, $capper= false, $capkg= false, $velocidad= false, $dcs= false, $elink= false, $pservicio= false, $gtecnica= false, $estadoins= false, $montosi= false, $montosn= false, $estado= false, $monitoreo= false, $lat= false, $lon= false, $iddirk= false, $condicion= '0,1', $created_time= false, $updated_time= false, $created_user= false, $updated_user= false, $closed_time= false ) {
    $query = $this->list($idascensor, $iduser, $idedificio, $idcontrato, $idventa, $idtascensor, $idcentrocosto, $marca, $modelo, $ubicacion, $codigo, $codigocli, $ken, $valoruf, $valorclp, $comando, $paradas, $accesos, $capper, $capkg, $velocidad, $dcs, $elink, $pservicio, $gtecnica, $estadoins, $montosi, $montosn, $estado, $monitoreo, $lat, $lon, $iddirk, $condicion, $created_time, $updated_time, $created_user, $updated_user, $closed_time);
  
    if ($row = db_fetch($query)) {

      $this->setIdascensor($row["idascensor"]);
      $this->setIduser($row["iduser"]);
      $this->setIdedificio($row["idedificio"]);
      $this->setIdcontrato($row["idcontrato"]);
      $this->setIdventa($row["idventa"]);
      $this->setIdtascensor($row["idtascensor"]);
      $this->setIdcentrocosto($row["idcentrocosto"]);
      $this->setMarca($row["marca"]);
      $this->setModelo($row["modelo"]);
      $this->setUbicacion($row["ubicacion"]);
      $this->setCodigo($row["codigo"]);
      $this->setCodigocli($row["codigocli"]);
      $this->setKen($row["ken"]);
      $this->setValoruf($row["valoruf"]);
      $this->setValorclp($row["valorclp"]);
      $this->setComando($row["comando"]);
      $this->setParadas($row["paradas"]);
      $this->setCapper($row["accesos"]);
      $this->setAccesos($row["capper"]);
      $this->setCapkg($row["capkg"]);
      $this->setVelocidad($row["velocidad"]);
      $this->setDcs($row["dcs"]);
      $this->setElink($row["elink"]);
      $this->setPservicio($row["pservicio"]);
      $this->setGtecnica($row["gtecnica"]);
      $this->setEstadoins($row["estadoins"]);
      $this->setMontosi($row["montosi"]);
      $this->setMontosn($row["montosn"]);
      $this->setEstado($row["estado"]);
      $this->setMonitoreo($row["monitoreo"]);
      $this->setLat($row["lat"]);
      $this->setLon($row["lon"]);
      $this->setCondicion($row["iddirk"]);
      $this->setIddirk($row["condicion"]);
      $this->setCreated_time($row["created_time"]);
      $this->setUpdated_time($row["updated_time"]);
      $this->setCreated_user($row["created_user"]);
      $this->setUpdated_user($row["updated_user"]);
      $this->setClosed_time($row["closed_time"]);
    }
  }


  /**
   * Get the value of idascensor
   */ 
  public function getIdascensor()
  {
    return $this->idascensor;
  }

  /**
   * Set the value of idascensor
   *
   * @return  self
   */ 
  public function setIdascensor($idascensor)
  {
    $this->idascensor = $idascensor;

    return $this;
  }

  /**
   * Get the value of iduser
   */ 
  public function getIduser()
  {
    return $this->iduser;
  }

  /**
   * Set the value of iduser
   *
   * @return  self
   */ 
  public function setIduser($iduser)
  {
    $this->iduser = $iduser;

    return $this;
  }

  /**
   * Get the value of idedificio
   */ 
  public function getIdedificio()
  {
    return $this->idedificio;
  }

  /**
   * Set the value of idedificio
   *
   * @return  self
   */ 
  public function setIdedificio($idedificio)
  {
    $this->idedificio = $idedificio;

    return $this;
  }

  /**
   * Get the value of idcontrato
   */ 
  public function getIdcontrato()
  {
    return $this->idcontrato;
  }

  /**
   * Set the value of idcontrato
   *
   * @return  self
   */ 
  public function setIdcontrato($idcontrato)
  {
    $this->idcontrato = $idcontrato;

    return $this;
  }

  /**
   * Get the value of idventa
   */ 
  public function getIdventa()
  {
    return $this->idventa;
  }

  /**
   * Set the value of idventa
   *
   * @return  self
   */ 
  public function setIdventa($idventa)
  {
    $this->idventa = $idventa;

    return $this;
  }

  /**
   * Get the value of idtascensor
   */ 
  public function getIdtascensor()
  {
    return $this->idtascensor;
  }

  /**
   * Set the value of idtascensor
   *
   * @return  self
   */ 
  public function setIdtascensor($idtascensor)
  {
    $this->idtascensor = $idtascensor;

    return $this;
  }

  /**
   * Get the value of idcentrocosto
   */ 
  public function getIdcentrocosto()
  {
    return $this->idcentrocosto;
  }

  /**
   * Set the value of idcentrocosto
   *
   * @return  self
   */ 
  public function setIdcentrocosto($idcentrocosto)
  {
    $this->idcentrocosto = $idcentrocosto;

    return $this;
  }

  /**
   * Get the value of marca
   */ 
  public function getMarca()
  {
    return $this->marca;
  }

  /**
   * Set the value of marca
   *
   * @return  self
   */ 
  public function setMarca($marca)
  {
    $this->marca = $marca;

    return $this;
  }

  /**
   * Get the value of modelo
   */ 
  public function getModelo()
  {
    return $this->modelo;
  }

  /**
   * Set the value of modelo
   *
   * @return  self
   */ 
  public function setModelo($modelo)
  {
    $this->modelo = $modelo;

    return $this;
  }

  /**
   * Get the value of ubicacion
   */ 
  public function getUbicacion()
  {
    return $this->ubicacion;
  }

  /**
   * Set the value of ubicacion
   *
   * @return  self
   */ 
  public function setUbicacion($ubicacion)
  {
    $this->ubicacion = $ubicacion;

    return $this;
  }

  /**
   * Get the value of codigo
   */ 
  public function getCodigo()
  {
    return $this->codigo;
  }

  /**
   * Set the value of codigo
   *
   * @return  self
   */ 
  public function setCodigo($codigo)
  {
    $this->codigo = $codigo;

    return $this;
  }

  /**
   * Get the value of codigocli
   */ 
  public function getCodigocli()
  {
    return $this->codigocli;
  }

  /**
   * Set the value of codigocli
   *
   * @return  self
   */ 
  public function setCodigocli($codigocli)
  {
    $this->codigocli = $codigocli;

    return $this;
  }

  /**
   * Get the value of ken
   */ 
  public function getKen()
  {
    return $this->ken;
  }

  /**
   * Set the value of ken
   *
   * @return  self
   */ 
  public function setKen($ken)
  {
    $this->ken = $ken;

    return $this;
  }

  /**
   * Get the value of valoruf
   */ 
  public function getValoruf()
  {
    return $this->valoruf;
  }

  /**
   * Set the value of valoruf
   *
   * @return  self
   */ 
  public function setValoruf($valoruf)
  {
    $this->valoruf = $valoruf;

    return $this;
  }

  /**
   * Get the value of valorclp
   */ 
  public function getValorclp()
  {
    return $this->valorclp;
  }

  /**
   * Set the value of valorclp
   *
   * @return  self
   */ 
  public function setValorclp($valorclp)
  {
    $this->valorclp = $valorclp;

    return $this;
  }

  /**
   * Get the value of comando
   */ 
  public function getComando()
  {
    return $this->comando;
  }

  /**
   * Set the value of comando
   *
   * @return  self
   */ 
  public function setComando($comando)
  {
    $this->comando = $comando;

    return $this;
  }

  /**
   * Get the value of paradas
   */ 
  public function getParadas()
  {
    return $this->paradas;
  }

  /**
   * Set the value of paradas
   *
   * @return  self
   */ 
  public function setParadas($paradas)
  {
    $this->paradas = $paradas;

    return $this;
  }

  /**
   * Get the value of capper
   */ 
  public function getCapper()
  {
    return $this->capper;
  }

  /**
   * Set the value of capper
   *
   * @return  self
   */ 
  public function setCapper($capper)
  {
    $this->capper = $capper;

    return $this;
  }

  /**
   * Get the value of accesos
   */ 
  public function getAccesos()
  {
    return $this->accesos;
  }

  /**
   * Set the value of accesos
   *
   * @return  self
   */ 
  public function setAccesos($accesos)
  {
    $this->accesos = $accesos;

    return $this;
  }

  /**
   * Get the value of capkg
   */ 
  public function getCapkg()
  {
    return $this->capkg;
  }

  /**
   * Set the value of capkg
   *
   * @return  self
   */ 
  public function setCapkg($capkg)
  {
    $this->capkg = $capkg;

    return $this;
  }

  /**
   * Get the value of velocidad
   */ 
  public function getVelocidad()
  {
    return $this->velocidad;
  }

  /**
   * Set the value of velocidad
   *
   * @return  self
   */ 
  public function setVelocidad($velocidad)
  {
    $this->velocidad = $velocidad;

    return $this;
  }

  /**
   * Get the value of dcs
   */ 
  public function getDcs()
  {
    return $this->dcs;
  }

  /**
   * Set the value of dcs
   *
   * @return  self
   */ 
  public function setDcs($dcs)
  {
    $this->dcs = $dcs;

    return $this;
  }

  /**
   * Get the value of elink
   */ 
  public function getElink()
  {
    return $this->elink;
  }

  /**
   * Set the value of elink
   *
   * @return  self
   */ 
  public function setElink($elink)
  {
    $this->elink = $elink;

    return $this;
  }

  /**
   * Get the value of pservicio
   */ 
  public function getPservicio()
  {
    return $this->pservicio;
  }

  /**
   * Set the value of pservicio
   *
   * @return  self
   */ 
  public function setPservicio($pservicio)
  {
    $this->pservicio = $pservicio;

    return $this;
  }

  /**
   * Get the value of gtecnica
   */ 
  public function getGtecnica()
  {
    return $this->gtecnica;
  }

  /**
   * Set the value of gtecnica
   *
   * @return  self
   */ 
  public function setGtecnica($gtecnica)
  {
    $this->gtecnica = $gtecnica;

    return $this;
  }

  /**
   * Get the value of estadoins
   */ 
  public function getEstadoins()
  {
    return $this->estadoins;
  }

  /**
   * Set the value of estadoins
   *
   * @return  self
   */ 
  public function setEstadoins($estadoins)
  {
    $this->estadoins = $estadoins;

    return $this;
  }

  /**
   * Get the value of montosi
   */ 
  public function getMontosi()
  {
    return $this->montosi;
  }

  /**
   * Set the value of montosi
   *
   * @return  self
   */ 
  public function setMontosi($montosi)
  {
    $this->montosi = $montosi;

    return $this;
  }

  /**
   * Get the value of montosn
   */ 
  public function getMontosn()
  {
    return $this->montosn;
  }

  /**
   * Set the value of montosn
   *
   * @return  self
   */ 
  public function setMontosn($montosn)
  {
    $this->montosn = $montosn;

    return $this;
  }

  /**
   * Get the value of estado
   */ 
  public function getEstado()
  {
    return $this->estado;
  }

  /**
   * Set the value of estado
   *
   * @return  self
   */ 
  public function setEstado($estado)
  {
    $this->estado = $estado;

    return $this;
  }

  /**
   * Get the value of monitoreo
   */ 
  public function getMonitoreo()
  {
    return $this->monitoreo;
  }

  /**
   * Set the value of monitoreo
   *
   * @return  self
   */ 
  public function setMonitoreo($monitoreo)
  {
    $this->monitoreo = $monitoreo;

    return $this;
  }

  /**
   * Get the value of lat
   */ 
  public function getLat()
  {
    return $this->lat;
  }

  /**
   * Set the value of lat
   *
   * @return  self
   */ 
  public function setLat($lat)
  {
    $this->lat = $lat;

    return $this;
  }

  /**
   * Get the value of lon
   */ 
  public function getLon()
  {
    return $this->lon;
  }

  /**
   * Set the value of lon
   *
   * @return  self
   */ 
  public function setLon($lon)
  {
    $this->lon = $lon;

    return $this;
  }

  /**
   * Get the value of condicion
   */ 
  public function getCondicion()
  {
    return $this->condicion;
  }

  /**
   * Set the value of condicion
   *
   * @return  self
   */ 
  public function setCondicion($condicion)
  {
    $this->condicion = $condicion;

    return $this;
  }

  /**
   * Get the value of iddirk
   */ 
  public function getIddirk()
  {
    return $this->iddirk;
  }

  /**
   * Set the value of iddirk
   *
   * @return  self
   */ 
  public function setIddirk($iddirk)
  {
    $this->iddirk = $iddirk;

    return $this;
  }

  /**
   * Get the value of created_time
   */ 
  public function getCreated_time()
  {
    return $this->created_time;
  }

  /**
   * Set the value of created_time
   *
   * @return  self
   */ 
  public function setCreated_time($created_time)
  {
    $this->created_time = $created_time;

    return $this;
  }

  /**
   * Get the value of updated_time
   */ 
  public function getUpdated_time()
  {
    return $this->updated_time;
  }

  /**
   * Set the value of updated_time
   *
   * @return  self
   */ 
  public function setUpdated_time($updated_time)
  {
    $this->updated_time = $updated_time;

    return $this;
  }

  /**
   * Get the value of created_user
   */ 
  public function getCreated_user()
  {
    return $this->created_user;
  }

  /**
   * Set the value of created_user
   *
   * @return  self
   */ 
  public function setCreated_user($created_user)
  {
    $this->created_user = $created_user;

    return $this;
  }

  /**
   * Get the value of updated_user
   */ 
  public function getUpdated_user()
  {
    return $this->updated_user;
  }

  /**
   * Set the value of updated_user
   *
   * @return  self
   */ 
  public function setUpdated_user($updated_user)
  {
    $this->updated_user = $updated_user;

    return $this;
  }

  /**
   * Get the value of closed_time
   */ 
  public function getClosed_time()
  {
    return $this->closed_time;
  }

  /**
   * Set the value of closed_time
   *
   * @return  self
   */ 
  public function setClosed_time($closed_time)
  {
    $this->closed_time = $closed_time;

    return $this;
  }
}