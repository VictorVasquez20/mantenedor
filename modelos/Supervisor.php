<?php 
class Supervisor {
    protected $idsupervisor;
    protected $idcargosup;
    protected $nombre;
    protected $apellido;
    protected $rut;
    protected $telefono_interno;
    protected $email_interno;


    public function __construct() {
        
    }
    public function list(){
        $sql = "SELECT idsupervisor, idcargosup, nombre, apellido, rut, telefono_interno, email_interno FROM supervisor order by nombre";
        return db_query($sql);
    }

    /**
     * Get the value of idsupervisor
     */ 
    public function getIdsupervisor()
    {
        return $this->idsupervisor;
    }

    /**
     * Set the value of idsupervisor
     *
     * @return  self
     */ 
    public function setIdsupervisor($idsupervisor)
    {
        $this->idsupervisor = $idsupervisor;

        return $this;
    }

    /**
     * Get the value of idcargosup
     */ 
    public function getIdcargosup()
    {
        return $this->idcargosup;
    }

    /**
     * Set the value of idcargosup
     *
     * @return  self
     */ 
    public function setIdcargosup($idcargosup)
    {
        $this->idcargosup = $idcargosup;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of apellido
     */ 
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set the value of apellido
     *
     * @return  self
     */ 
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get the value of rut
     */ 
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set the value of rut
     *
     * @return  self
     */ 
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get the value of telefono_interno
     */ 
    public function getTelefono_interno()
    {
        return $this->telefono_interno;
    }

    /**
     * Set the value of telefono_interno
     *
     * @return  self
     */ 
    public function setTelefono_interno($telefono_interno)
    {
        $this->telefono_interno = $telefono_interno;

        return $this;
    }

    /**
     * Get the value of email_interno
     */ 
    public function getEmail_interno()
    {
        return $this->email_interno;
    }

    /**
     * Set the value of email_interno
     *
     * @return  self
     */ 
    public function setEmail_interno($email_interno)
    {
        $this->email_interno = $email_interno;

        return $this;
    }
}
