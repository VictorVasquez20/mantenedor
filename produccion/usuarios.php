<?php

require 'header.php';

if (!isset($_SESSION["nombre"])) {
	header("Location:login.php");
} else {
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Usuarios APP FAB</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                                    </li>
                                    <li><a id="op_listar" onclick="mostarform(false)">LISTAR</a>
                                    </li>
                                </ul>
                            </li>               
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    
                    </div>
                    <div id="listadousuarios">
                        <table id="tablaUser" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>OPCIONES</th>
                                    <th>NOMBRE DE USUARIO</th>
                                    <th>NOMBRE Y APELLIDO</th>
                                    <th>DOCUMENTO IDENTIDAD</th>
                                    <th>EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="formulariousuario" class="x_content">
                        <div class="x_title col-md-12 col-sm-12 col-xs-12">
                                    <h4>Datos Usuarios</h4>
                        </div>
                        <br />
                        <form id="formulario" name="formulario"  class="form-horizontal form-label-left input_mask">
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <label>TIPO DE DOCUMENTO</label>  
                                <select class="form-control selectpicker" data-live-search="true" id="tipo_documento" name="tipo_documento" required="required">
                                    <option value="" selected disabled>Tipo de Documento</option>
                                    <option value="RUT">RUT</option>
                                    <option value="P">Pasaporte</option>
                                    <option value="O">Otro</option>
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <label>NUMERO DE DOCUMENTO</label>  
                                <input type="text" class="form-control has-feedback-left" id="num_documento" name="num_documento" placeholder="Numero de Documento" required="required"  maxlength="40">
                                <span class="fa fa-id-card form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <label>Username</label>  
                            <input type="text" class="form-control has-feedback-left" id="username" name="username" placeholder="Username" required="required">
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                            <label>Contraseña</label> 
                            <input type="hidden" class="form-control has-feedback-left" id="password_v" name="password_v" placeholder="Contraseña" required="required">
                            <input type="password" class="form-control has-feedback-left" id="password" name="password" placeholder="Contraseña" required="required">
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback" id="div_check">
                            <br/>
                            <br/>
                            <input type="checkbox" class="js-switch" id="ckpass" name="ckpass" >
                        
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="hidden" class="form-control has-feedback-left" id="iduser" name="iduser">
                                <input type="hidden" class="form-control has-feedback-left" id="idtecnico" name="idtecnico">
                                
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Nombre</label>
                                <input type="text" class="form-control has-feedback-left" id="nombre" name="nombre" placeholder="Nombres" required="Campo requerido" >
                                <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Apellido</label>
                            <input type="text" class="form-control has-feedback-left" id="apellido" name="apellido" placeholder="Apellidos" required="Campo requerido">
                            <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Contacto</label>
                                <input type="text" id="movil" name="movil" class="form-control has-feedback-left" placeholder="Telefono Movil" data-inputmask="'mask' : '+56(9)9999-9999'" required="Campo requerido">
                                <span class="fa fa-mobile spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                            <label>Correo</label>
                                <input type="text" class="form-control has-feedback-left" id="email_corporativo" style="text-transform: lowercase" name="email_corporativo" placeholder="Email Corporativo" required="Campo requerido">
                                <span class="fa fa-envelope-o  spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                  <label>FECHA DE NACIMIENTO</label> 
                                <input type='date' id="fecha_nac" name="fecha_nac" class="form-control has-feedback-left" placeholder="Fecha de Nacimiento" required="Campo requerido"/> 
                                <span class="fa fa-calendar  spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <label>Dirección</label>
                                <input type="text" class="form-control has-feedback-left" id="direccion" name="direccion" placeholder="Direccion" required="Campo requerido">
                                <span class="fa fa-map-marker  form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>ROL</label>  
                                <select class="form-control selectpicker" data-live-search="true" id="idrol" name="idrol" required="required" onchange="hideSelected()" disabled>
                                </select>
							</div>  
                            <div id="divSupervisor" class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Supervisor</label>  
                                <select class="form-control selectpicker" data-live-search="true" id="idsupervisor" name="idsupervisor" required="required" disabled>
                                <option value="" selected></option>
                                </select>
							</div>    
                            <div id="divCargo" class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>CARGO</label>  
                                <select class="form-control selectpicker" data-live-search="true" id="idcategoria" name="idcategoria" required="required" disabled>
                                <option value="" selected></option>
                                </select>
							</div>  
                           
                            </br>
                            </br>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <button class="btn btn-primary" type="reset" id="btnVolver" onclick="volver()">Volver</button>
                                    <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                    <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                </div>
                            </div>
                        </form>
                        </div>
                </div>
            </div>
        </div>                   
    </div>
</div>
<?php 
    require 'footer.php';
?>
<?php
    echo '<script type=text/javascript src="scripts/usuario.js?'.$_SESSION["version"].'"></script>';
?>
   
    <?php
}

ob_end_flush();