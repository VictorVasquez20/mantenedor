        <footer>
          <div class="pull-right">
              SISTEMA <a target="_blank" href="http://www.fabrimetal.cl/">FABRIMETAL S.A.<i class="fa fa-registered"></i></a> 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../public/build/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../public//build/js/bootstrap.min.js"></script>
    <!-- Chart.js -->
    <script src="../public/build/js/Chart.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../public/build/js/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../public/build/js/icheck.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="../public/build/js/moment.min.js"></script>
    <script src="../public/build/js/daterangepicker.js"></script>
    
    <!-- FullCalendar --> 
    <!-- DTR.INI - 06-04-2020
        Correccion de error que se muestra en consola, al parecer no encuentra una funcion.
     -->
    <script src="../public/build/js/fullcalendar.min.js"></script>
    <!-- <script src="../public/build/js/es.js"></script> -->
     

    <!-- bootstrap-fileinput -->
    <script src="../public/build/js/fileinput.min.js"></script>
    <!-- bootstrap-select -->
    <script src="../public/build/js/bootstrap-select.min.js"></script>

    <!-- bootstrap-datetimepicker -->    
    <script src="../public/build/js/bootstrap-datetimepicker.min.js"></script>
   <!-- Bootstrap Colorpicker -->
    <script src="../public/build/js/bootstrap-colorpicker.min.js"></script>

    <!-- Datatables -->
    <script src="../public/build/js/jquery.dataTables.min.js"></script>
    <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
    <script src="../public/build/js/dataTables.buttons.min.js"></script>
    <script src="../public/build/js/buttons.bootstrap.min.js"></script>
    <script src="../public/build/js/buttons.flash.min.js"></script>
    <script src="../public/build/js/buttons.html5.min.js"></script>
    <script src="../public/build/js/buttons.print.min.js"></script>
    <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
    <script src="../public/build/js/dataTables.keyTable.min.js"></script>
    <script src="../public/build/js/dataTables.responsive.min.js"></script>
    <script src="../public/build/js/responsive.bootstrap.js"></script>
    <script src="../public/build/js/dataTables.scroller.min.js"></script>
    <script src="../public/build/js/jszip.min.js"></script>
    <script src="../public/build/js/pdfmake.min.js"></script>
    <script src="../public/build/js/vfs_fonts.js"></script>

    <!-- Bootbox Alert -->
    <script src="../public/build/js/bootbox.min.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../public/build/js/jquery.tagsinput.js"></script>

    <!-- jquery.inputmask -->
    <script src="../public/build/js/jquery.inputmask.bundle.min.js"></script>

    <!-- bootstrap-wysiwyg -->
    <script src="../public/build/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../public/build/js/jquery.hotkeys.js"></script>
    <script src="../public/build/js/prettify.js"></script>
    
    <!-- PNotify -->
    <script src="../public/build/js/pnotify.js"></script>
    <script src="../public/build/js/pnotify.buttons.js"></script>
    <script src="../public/build/js/pnotify.nonblock.js"></script>
    
    <!-- Switchery -->
    <script src="../public/build/js/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../public/build/js/select2.full.min.js"></script>
    
    <!-- Autosize -->
    <script src="../public/build/js/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../public/build/js/jquery.autocomplete.min.js"></script>

    
    <!-- morris.js -->
    <script src="../public/build/js/raphael.min.js"></script>
    <script src="../public/build/js/morris.min.js"></script>
    
    <!-- Flot -->
    <script src="../public/build/js/jquery.flot.js"></script>
    <script src="../public/build/js/jquery.flot.pie.js"></script>
    <script src="../public/build/js/jquery.flot.time.js"></script>
    <script src="../public/build/js/jquery.flot.stack.js"></script>
    <script src="../public/build/js/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../public/build/js/jquery.flot.orderBars.js"></script>
    <script src="../public/build/js/jquery.flot.spline.min.js"></script>
    <script src="../public/build/js/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../public/build/js/date.js"></script>
    <script src="../public/sweet/dist/sweetalert2.min.js"></script>
    <!-- Sweet -->
    

    <script type="text/javascript" src="../public/build/js/echarts.min.js"></script>
    
    <script src="../public/build/js/multi-select/jquery.multi-select.js"></script>
      <!-- Custom Theme Scripts -->
    <script src="../public/build/js/custom.js"></script>
  </body>
</html>