function init(){
   localStorage.clear()
    mostarform(false)
    getContact()
    $('#buscadorDiv').hide()
    $('#crearDiv').hide()
    $('#formularioGuardar').hide()
    $("#formulario").on("submit", function(e){
		buscador(e);
    });
    $("#formularioCrear").on("submit", function(e){
		countUser(e);
    });
    $("#EliminarDiv").on("submit", function(e){
        divEliminar(e);
    });
   $("#formularioGuardar").on("submit", function(e){
      guardar(e);
   });
   $("#formularioEdit").on("submit", function(e){
      editar(e);
   });
   $(document).ready(function(){
    /***phone number format***/
    $(".phone-format").keypress(function (e) {
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
      }
      var curchr = this.value.length;
      var curval = $(this).val();
      if (curchr == 3 && curval.indexOf("(") <= -1) {
        $(this).val("(" + curval + ")" + "-");
      } else if (curchr == 4 && curval.indexOf("(") > -1) {
        $(this).val(curval + ")-");
      } else if (curchr == 5 && curval.indexOf(")") > -1) {
        $(this).val(curval + "-");
      } else if (curchr == 9) {
        $(this).val(curval + "-");
        $(this).attr('maxlength', '14');
      }
    });
  });
   
}
function getContact(){
    tablaContacto = $('#tablaContacto').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/contactoAjax.php?exe=getListContact',
            type: "POST",
            dataType: "json",
            error: function (e) {
              $('#tablaContacto').hide();
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[4, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}
function limpiar(){
  $('#buscar').val('')
  $('#count').val('')
  $('#buscar').val('')
  $('#buscar').val('')
  $('#buscadorDiv').hide()
  $('#crearDiv').hide()
  $('#formularioGuardar').hide()
  
}
function mostarform(flag){
    limpiar();
    $('#divEdit').hide()
    if(flag){
        $("#listadocontacto").hide()
        $("#formulariocontacto").show()
        $("#op_agregar").hide()
        $("#op_listar").show()
        $("#btnGuardar").prop("disabled", true)
    }else{
        $("#listadocontacto").show();
        $("#formulariocontacto").hide()
        $("#op_agregar").show()
        $("#op_listar").hide()
        $("#btnGuardar").prop("disabled", true)
    }

}

function buscador (e){
    e.preventDefault()
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
		url:'../ajax/contactoAjax.php?exe=buscador',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,
		success: function(datos){
      let data = JSON.parse(datos)
      localStorage.setItem('Edificio',JSON.stringify(data))
      if(data.length > 0){
        let template = ""
        data.forEach(elemet => {
          template += `<div class="col-md-12 col-sm-12 col-xs-12"><input type="checkbox" class="js-switch" id='${elemet.idedificio}' name="ckpass" /> ${elemet.nombre}</div>`
        })
          $('#buscadorDiv').show()
          $('#crearDiv').show()
          $('#buscadorDiv').html(template) 
        }else{
          Swal.fire({
            title: 'Advertencia',
            text: 'No existe edificio. Ingrese una nueva búsqueda',
            icon: 'warning',
          })
        }
      }
    });
}

function countUser (e){
    e.preventDefault()
    let template = ``
    for (let index = 0; index < $('#count').val(); index++) {
       template += `<div id='blone' style="margin-bottom: 30px;">
                    <div class="x_title">
                        <h2>${index +1}.Contacto</h2>
                        <div class="clearfix"></div>
                    </div>
                     <div class='col-md-6 col-sm-6 col-xs-6 form-group has-feedback'>
                       <label>Nombre</label>
                       <input type="text" class="form-control has-feedback-left" id="nombre${index +1}" name="nombre" placeholder="Ingresar nombre" required="Campo requerido">
                       <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                     </div>
                     <div class='col-md-6 col-sm-6 col-xs-6 form-group has-feedback'>
                       <label>Cargo</label>
                       <input type="text" class="form-control has-feedback-left" id="cargo${index +1}" name="cargo" placeholder="Ingresar cargo" required="Campo requerido">
                       <span class="fa fa-building-o spn form-control-feedback left" aria-hidden="true"></span>
                     </div>
                     <div class='col-md-4 col-sm-4 col-xs-4 form-group has-feedback'>
                       <label>Correo</label>
                       <input type="email" class="form-control has-feedback-left" id="email${index +1}" name="email" placeholder="Ingresar correo" required="Campo requerido">
                       <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                     </div>
                     <div class='col-md-4 col-sm-4 col-xs-4 form-group has-feedback'>
                       <label>Teléfono Móvil</label>
                       <input type="number" class="form-control has-feedback-left" id="telMovil${index +1}" name="telMovil" placeholder="Ingresar teléfono móvil" data-inputmask="'mask' : '+56(9)9999-9999'" max="999999999">
                       <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                     </div>
                     <div class='col-md-4 col-sm-4 col-xs-4 form-group has-feedback'>
                       <label>Teléfono Fijo</label>
                       <input type="text" class="form-control has-feedback-left" id="telFijo${index +1}" name="telFijo" placeholder="Ingresar teléfono fijo" data-inputmask="'mask' : '+56(2)9999-9999'" max="999999999">
                       <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                     </div>
                   </div>`
       
        }
    $('#formularioGuardar').show()
    $('#crearContacto').html(template)
}

function divEliminar(e){
  e.preventDefault()
}

function guardar(e){
  e.preventDefault()
  let idBuscar = []
  $('#buscadorDiv').find('input[type=checkbox]:checked').each(function(){
    idBuscar.push(this.id)
  })
  if(idBuscar.length > 0) {
    let contacto = []
    let contact ={}
    $('#crearContacto').find('input').each(function(){
      if(this.name === 'telFijo' || this.name === 'telMovil'){
        let corteContact = $('#'+this.id).val()
        let corteContactEdit = corteContact.charAt(0)
        corteContact = `56+(${corteContactEdit})${corteContact.charAt(1)}${corteContact.charAt(2)}${corteContact.charAt(3)}${corteContact.charAt(4)}${corteContact.charAt(5)}${corteContact.charAt(6)}${corteContact.charAt(7)}${corteContact.charAt(8)}`
        contact = {
          ...contact,
          [this.name]: corteContact,
        }
      }else{
        contact = {
          ...contact,
          [this.name]: `${$('#'+this.id).val()}`,
        }
      }
      if(this.name === 'telFijo'){
        contacto.push(contact)
      }
    })
    let guardar = {
      idBuscar,
      contacto
    }
    $.ajax({
		  url:'../ajax/contactoAjax.php?exe=guardar',
      type:"POST",
		  data:guardar,
      dataType: "text",
		  success: function(datos){
        let data = JSON.parse(datos)
        let edificios = JSON.parse(localStorage.getItem('Edificio'))
        let mensaje = 'El correo ya existe en los siguientes edificio: </br></br>'
        let mensajeGuardado = '</br></br>Los datos en los siguientes edificios se guardaron correctamente: </br></br>'
        let datoSinError = true
        let datosGuardadosConerror = false
        data.forEach(e => {
          edificios.forEach(dataEdificio => {
            if(e.idEdificio !== undefined){
              if(dataEdificio.idedificio === e.idEdificio){
                mensaje += ' '+ '<p style{font-size: 20 !important;}><strong>'+ dataEdificio.nombre + '</strong></p>' + '\n'
              }
              datoSinError = false
            }
            
            if(e.idEdificiosGuardados !== undefined){
              if(e.idEdificiosGuardados === dataEdificio.idedificio){
                  mensajeGuardado += ' '+ '<p style{font-size: 20 !important;}><strong>'+ dataEdificio.nombre + '</strong></p>' + '\n'
                  datosGuardadosConerror = true
              }
            }
          })
        })
        if(datoSinError){
          Swal.fire({
            title: 'Datos guardados sin problemas',
            html: mensajeGuardado,
            icon: 'success',
          })
          mostarform(false);
            limpiar();
            tablaContacto.ajax.reload(null, true);
        }
        else if (datosGuardadosConerror){
          Swal.fire({
            title: 'Advertencia',
            html: mensaje + mensajeGuardado,
            icon: 'warning',
          })
          mostarform(false);
            limpiar();
            tablaContacto.ajax.reload(null, true);
        }else{
          Swal.fire({
            title: 'Advertencia',
            html: mensaje,
            icon: 'warning',
          })
        }
       /* bootbox.alert(datos);
          if(datos !== 'ERROR AL INSERTAR')
          {
            mostarform(false);
            limpiar();
            tablaContacto.ajax.reload(null, true);
          }*/
        }
    });
  }else{
    bootbox.alert('DEBE SELECCIONAR UN EDIFICIO');
  }
 
}

function mostrar(id){
  $.ajax({
    url:'../ajax/contactoAjax.php?exe=mostrar',
    type:"POST",
    data:{id},
    dataType: "text",
    success: function(datos){
      let data = JSON.parse(datos)
      $('#divEdit').show()
      $('#buscadorDiv').hide()
      $('#crearDiv').hide()
      $('#formularioGuardar').hide()
      $("#listadocontacto").hide()
      $("#op_agregar").hide()
      $("#op_listar").show()

      $('#nombreEdit').val(data.nombre)
      $('#idEdit').val(data.idcontacto)
      $('#cargoEdit').val(data.cargo)
      $('#emailEdit').val(data.email)
      $('#telMovilEdit').val(data.telefonomovil)
      $('#telFijoEdit').val(data.telefonoresi)
    }
  });
  
}

function desactivar(id){
  Swal.fire({
    title: 'Esta seguro que quiere eliminar el contacto?',
    showDenyButton: true,
    confirmButtonText: `Eliminar`,
    denyButtonText: `Volver`,
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      $.ajax({
        url:'../ajax/contactoAjax.php?exe=deleteContacto',
        type:"POST",
        data:{idcontacto: id},
        dataType: "text",
        success: function(datos){
          Swal.fire(datos, '', 'success')
            if(datos !== 'ERROR AL INSERTAR')
            {
              mostarform(false);
              tablaContacto.ajax.reload(null, true);
            }
          }
      });
    } else if (result.isDenied) {
      Swal.fire('No se elimino el contacto ', '', 'info')
    }
  })
  
}
function limpiarEdit(){
  $('#nombreEdit').val('')
  $('#cargoEdit').val('')
  $('#emailEdit').val('')
  $('#telMovilEdit').val('')
  $('#telFijoEdit').val('')
}
function editar(e){
  e.preventDefault()    
  var formData = new FormData($("#formularioEdit")[0]);
  $.ajax({
    url:'../ajax/contactoAjax.php?exe=editar',
    type:"POST",
    data:formData,
		contentType: false,
		processData:false,
    success: function(datos){
      if(datos !== 'ERROR AL INSERTAR')
      {
        Swal.fire({
          title: 'Datos Editados',
          icon: 'success',
        })
        mostarform(false);
        tablaContacto.ajax.reload(null, true);
      }else{
        Swal.fire({
          title: 'Error al editar',
          icon: 'error',
        })
      }  
    }
  });
}
function volver() {
  mostarform(false)
}
init()
