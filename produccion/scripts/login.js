$(document).on('submit', '.form_login', function (e) {
	e.preventDefault();

	$.ajax({
		type: "POST",
		url: "../ajax/login.php?exe=login",
		data: $(this).serialize(),
		dataType: "text",
		success: function (e) {	
			var data = JSON.parse(e)
				if (data.cod === 200) {
					window.location = 'usuarios.php';
				}else{
					bootbox.alert(data.mensaje);
				}
			}
		
	});


});
