var tabla= $('#tablaGuia');
function init(){
  //getGuia()
  $("#sinDatos").hide()
  $("#editarGuia").hide()
  $("#formulario").on("submit", function(e){
		editar(e);
  });
  $("#formularioBuscar").on("submit", function(e){
		getGuia(e);
  });
  getSelect()
}
function getGuia (e) {
  e.preventDefault()
    let mes = $("#mes").val();
    let ano = $("#ano").val();
    let guia;
    if ($("#gse").val() == null || $("#gse").val() == '' || $("#gse").val() == undefined){
      guia = 0;
    }
    else{
      guia = $("#gse").val();
    }    
    let fechaDesde = `${ano}-${mes}-1`;
    if (parseInt(mes) == 12){
      var fechaHasta = `${ano}-${parseInt(mes)}-31`;
    }
    else{
      var fechaHasta = `${ano}-${parseInt(mes)+1}-1`;
    }
  tablaGuia = $('#tablaGuia').dataTable({
      "aProcessing": true,
      "aServerSide": true,
      dom: 'Bfrtip',
      buttons: Botones,
      "language": Español,
      "ajax": {
          url: '../ajax/mantenedorGESAjax.php?exe=getListGuia',
          type: "POST",
          data: { fechaDesde, fechaHasta, guia},
          dataType: "json",
          dataSrc: function (e){
            if(e.length === 0){
              return ""
            }
            return e.aaData
          },
          error: function (e) {
              $('#listadoGuia').hide();
          },
      },
      "bDestroy": true,
      "iDisplayLength": 10, //Paginacion 10 items
      "order": [[1, "asc"]] //Ordenar en base a la columna 0 descendente
  }).DataTable();
}
function getSelect(){
  $.post('../ajax/mantenedorGESAjax.php?exe=getAscensor',function(e){
    $('#codigoFm').html(e);
    $("#codigoFm").selectpicker('refresh');
  })

  $.post('../ajax/mantenedorGESAjax.php?exe=getTServicio',function(e){
    $('#tipoServicio').html(e);
    $("#tipoServicio").selectpicker('refresh');
  })
  $.post('../ajax/mantenedorGESAjax.php?exe=getEstado',function(e){
    $('#estadoInicio').html(e);
    $("#estadoInicio").selectpicker('refresh');
    $('#estadoFin').html(e);
    $("#estadoFin").selectpicker('refresh');
  })
}
function mostrar(id){
  $('#filtroguias').hide()
  $('#listadoGuia').hide()
  $("#editarGuia").show()
  localStorage.clear()
  $.post("../ajax/mantenedorGESAjax.php?exe=mostrar",{id}, function(data){
    data = JSON.parse(data)
    $("#idservicio").val(data.idservicio)
    $("#idserviciol").html(data.idservicio)
    $("#codigoFm").val(data.codigoFm).trigger('change')
    $("#tipoServicio").val(data.tipoServicio).trigger('change')
    $("#estadoInicio").val(data.estadoInicio).trigger('change')
    $("#estadoFin").val(data.estadoFin).trigger('change')
    $("#obsInicio").val(data.obsInicio)
    $("#obsFin").val(data.obsFin)
    
    localStorage.setItem('data',JSON.stringify(data))
    $("#codigoFm").selectpicker('refresh')
    $("#tipoServicio").selectpicker('refresh')
    $("#estadoInicio").selectpicker('refresh')
    $("#estadoFin").selectpicker('refresh')

    $("#btnEditar").prop("disabled", false)
  });
}
function volver() {
  $('#filtroguias').show()
  $('#listadoGuia').show()
  $("#editarGuia").hide()
}
function editar(e){
  e.preventDefault()
  var formData = new FormData($("#formulario")[0])
  $.ajax({
		url:'../ajax/mantenedorGESAjax.php?exe=editar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,
		success: function(datos){
      let data = JSON.parse(localStorage.getItem('data'))
      let mensaje =  datosCambiados()
        if(datos === 'DATOS DEL SERVICIO ACTUALIZADO'){
          bootbox.alert(`<h4>${datos} - N°${data.idservicio}</h4></br> ${mensaje}`);
          tablaGuia.ajax.reload(null, true);
          $('#filtroguias').show()
          $('#listadoGuia').show()
          $("#editarGuia").hide()
          localStorage.clear()
         
        }else{
          bootbox.alert(datos)
        }
      }
    });
}
function datosCambiados() {
  let data = JSON.parse(localStorage.getItem('data'))
  let mensaje = `<h5>Datos Modificados</h5> </br>`
  let sinModificar = true;
  if(data.codigoFm !== $('#codigoFm').val() && data.codigoFm !== undefined){
    mensaje +=`<p>Codigo Fm: ${$('#codigoFm option:selected').text()}</p>`
    sinModificar = false
  }
  if(data.tipoServicio !== $('#tipoServicio').val() && data.tipoServicio !== undefined){
    mensaje +=`<p>Tipo de Servicio: ${$('#tipoServicio option:selected').text()}</p>`
    sinModificar = false
  }
  if(data.estadoInicio !== $('#estadoInicio').val() && data.estadoInicio !== undefined){
    mensaje +=`<p>Estado Inicial: ${$('#estadoInicio option:selected').text()}</p>`
    sinModificar = false
  }
  if(data.estadoFin !== $('#estadoFin').val() && data.estadoFin !== undefined){
    mensaje +=`<p>Estado Final: ${$('#estadoFin option:selected').text()}</p>`
    sinModificar = false
  }
  let saltoinicio = $('#obsInicio').val().replace(/(\r\n|\n|\r)/gm, "")
  let saltoinicioedit = data.obsInicio.replace(/(\r\n|\n|\r)/gm, "")
  if(saltoinicioedit !== saltoinicio && data.obsInicio !== undefined){
    mensaje +=`<p><u>Observación Inicial:</u></p>
               <p>${$('#obsInicio').val()}</p>`
    sinModificar = false
  }
  let saltofin = $('#obsFin').val().replace(/(\r\n|\n|\r)/gm, "")
  let saltofinedit = data.obsFin.replace(/(\r\n|\n|\r)/gm, "")
  if(saltofinedit !== saltofin && data.obsFin !== undefined){
    console.log(data.obsFin)
    console.log($('#obsFin').val())
    mensaje +=`<p><u>Observación Final:</u></p>
               <p>${$('#obsFin').val()}</p>`
    sinModificar = false
  }
  if(sinModificar){
    mensaje = `<h5>Datos Sin Modificaciones</h5> </br>`
  }
  return mensaje
  
  
}

function hiddeTable(){
  if(tablaGuia.data().count()==4){
    $('#tablaGuia').hide();
     
  }
  
}

init()

