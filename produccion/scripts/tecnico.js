var tabla;
function init(){
    mostarform(false)
    rutformat()
    getTecnico()
    getSupervisor()
    getCategoTec()
    $("#formulario").on("submit", function(e){
		submitTecnico(e);
    });
}
function getSupervisor(){
    $.post('../ajax/tecnicoAjax.php?exe=getSupervisor',function(e){
        $('#idsupervisor').html(e);
        $("#idsupervisor").selectpicker('refresh');
    })
}
function getCategoTec(){
    $.post('../ajax/tecnicoAjax.php?exe=getCategoTec',function(e){
        $('#idcargotec').html(e);
        $("#idcargotec").selectpicker('refresh');
    })
}
function getTecnico(){
    tablaUser = $('#tablatecnico').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/tecnicoAjax.php?exe=getListTecnico',
            type: "POST",
            dataType: "json",
            error: function (e) {
                $('#tablatecnico').hide();
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}
function rutformat(){
    $("#num_documento").focusout(function () {
        var regexp = /^\d{2}.\d{3}.\d{3}-[k|K|\d]{1}$/;

            if( ( $("#tipo_documento").val()== 'RUT' && regexp.test($("#num_documento").val()) ) ||
                 ( $("#tipo_documento").val()!= 'RUT' && $("#num_documento").val().length > 0 ) ){  
                    
                    $.post("../ajax/usuarioAjax.php?exe=validarExisteNumDocumento",{"num_documento": $("#num_documento").val()}, function(data){
                        let { datos, datosTecnico, userData, tecData } = JSON.parse(data)
                        console.log(datosTecnico)
                        if(parseInt(datos.cantidad)> 0 && parseInt(datosTecnico.cantidad)> 0){
                            //active()
                            //$("#btnGuardar").prop("disabled", false);
                            if(tecData !==undefined){
                                if(tecData.condicion === "0"){
                                    desactive();
                                    $('#idtecnico').val(tecData.idtecnico)
                                    $("#nombre").val(userData.nombre);
                                    $("#apellido").val(userData.apellido);
                                    $("#movil").val(userData.telefono);
                                    $('#email_corporativo').val(userData.email);
                                    $("#idsupervisor").val(tecData.idsupervisor).trigger('change');
                                    $("#idcargotec").val(tecData.idcargotec).trigger('change');
                                    $("#idsupervisor").selectpicker('refresh');
                                    $("#idcargotec").selectpicker('refresh');
                                    $("#btnGuardar").removeClass('btn-success');
                                    $("#btnGuardar").prop("disabled", false);
                                    $("#btnGuardar").addClass('btn-warning');
                                    $("#btnGuardar").text('Activar');
                                    
                                    new PNotify({
                                        title: 'Advertencia!',
                                        text: 'Usuario desactivado.',
                                        type: 'warning',
                                        styling: 'bootstrap3'
                                    }); 
                                }
                            }else{
                                $("#btnGuardar").addClass('btn-success')
                                $("#btnGuardar").prop("disabled", true);
                                desactive()
                                $("#nombre").val("");
                                $("#apellido").val("");
                                $("#movil").val("");
                                $('#email_corporativo').val("");
                                $("#idsupervisor").val("");
                                $("#idcargotec").val("");
                                $("#idsupervisor").selectpicker('refresh');
                                $("#idcargotec").selectpicker('refresh');
                                $("#btnGuardar").text('Agregar');
                                new PNotify({
                                    title: 'Error!',
                                    text: 'Usuario ya existe.',
                                    type: 'error',
                                    styling: 'bootstrap3'
                                }); 
                            }
                        }else if(parseInt(datos.cantidad)> 0 && parseInt(datosTecnico.cantidad) === 0){
                            $("#nombre").val(userData.nombre);
                            $("#apellido").val(userData.apellido);
                            $("#movil").val(userData.telefono);
                            $('#email_corporativo').val(userData.email);
                            active()
                            $("#btnGuardar").addClass('btn-success')
                            $("#btnGuardar").prop("disabled", false);
                            $("#idsupervisor").val("");
                            $("#idcargotec").val("");
                            $("#idsupervisor").selectpicker('refresh');
                            $("#idcargotec").selectpicker('refresh');
                            $("#btnGuardar").text('Agregar');    
                            new PNotify({
                                title: 'Success!',
                                text: 'Ingrese sus datos.',
                                type: 'success',
                                styling: 'bootstrap3'
                            }); 
                        }else if(parseInt(datos.cantidad)=== 0){
                            desactive()
                            $("#btnGuardar").addClass('btn-success')
                            $("#btnGuardar").prop("disabled", true);
                            $("#nombre").val("");
                            $("#apellido").val("");
                            $("#movil").val("");
                            $('#email_corporativo').val("");
                            $("#idsupervisor").val("");
                            $("#idcargotec").val("");
                            $("#idsupervisor").selectpicker('refresh');
                            $("#idcargotec").selectpicker('refresh');
                            $("#btnGuardar").text('Agregar');  
                            new PNotify({
                                title: 'Error!',
                                text: 'Usuario no existe.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });  
                        }
                    })
           }else{
            $("#btnGuardar").addClass('btn-success')
                $("#btnGuardar").prop("disabled", true);
                $("#nombre").val("");
                $("#apellido").val("");
                $("#movil").val("");
                $('#email_corporativo').val("");
                $("#idsupervisor").val("");
                $("#idcargotec").val("");
                $("#idsupervisor").selectpicker('refresh');
                $("#idcargotec").selectpicker('refresh');
                $("#btnGuardar").text('Agregar');               
                new PNotify({
                    title: 'Error!',
                    text: 'Debe completar el numero.',
                    type: 'error',
                    styling: 'bootstrap3'
                });  
               
           }

   });
}
function mostarform(flag){

    limpiar();
    
	if(flag){
		$("#listadotecnico").hide();
		$("#formulariotecnico").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", true);

	}else{
        desactive()
		$("#listadotecnico").show();
		$("#formulariotecnico").hide();
		$("#op_agregar").show();
        $("#op_listar").hide();
        $("#btnGuardar").prop("disabled", true)
	}

}
function limpiar(){
	$("#iduser").val("");	
	$("#nombre").val("");
    $("#apellido").val("");
	$("#num_documento").val("");
    $("#num_documento").inputmask('remove');
    $("#num_documento").inputmask({"mask": "99.999.999-*"}); //specifying options
    $("#num_documento").attr('readonly', false);
	$("#direccion").val("");
	$("#movil").val("");	
    $("#email_corporativo").val("");
    $("#idsupervisor").val("")
    $("#idcargotec").val("")
    $("#idsupervisor").selectpicker('refresh')
    $("#idcargotec").selectpicker('refresh')
}
function desactive(){
    $("#nombre").prop('disabled', true);
    $("#apellido").prop('disabled', true);
    $("#idsupervisor").prop('disabled', true)
    $("#fecha_nac").prop('disabled', true);
    $("#direccion").prop('disabled', true);
    $("#movil").prop('disabled', true);
    $('#email_corporativo').prop('disabled', true);
    $('#password_v').prop('disabled', true);
    $("#idcargotec").prop('disabled', true);
 
}
function active(){
    $("#nombre").removeAttr('disabled');
    $("#apellido").removeAttr('disabled');
    $("#username").removeAttr('disabled');
    $("#fecha_nac").removeAttr('disabled');
    $("#direccion").removeAttr('disabled');
    $("#movil").removeAttr('disabled');
    $('#email_corporativo').removeAttr('disabled');
    $("#idsupervisor").removeAttr('disabled');
    $("#idcargotec").removeAttr('disabled');
    //$("#password").removeAttr('disabled');
    $('#div_check').show();
    $("#idcargotec").selectpicker('refresh')
    $("#idsupervisor").selectpicker('refresh')


}
function submitTecnico(e){
    e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    if($("#btnGuardar").hasClass("btn-warning")){
        let idtecnico =$('#idtecnico').val()
        $.post("../ajax/tecnicoAjax.php?exe=activar",{idtecnico}, function(e){
            bootbox.alert(e);
            mostarform(false);
            tablaUser.ajax.reload(null, true);
            desactive();
        });
    }else{
        $.ajax({
            url:'../ajax/tecnicoAjax.php?exe=guardar',
            type:"POST",
            data:formData,
            contentType: false,
            processData:false,
            success: function(datos){
                bootbox.alert(datos);
                if(datos !== 'ERROR AL INSERTAR')
                {
                    mostarform(false);
                    tablaUser.ajax.reload(null, true);
                    desactive();
                    //tabla.ajax.reload();
                }else{
                    $("#btnGuardar").prop("disabled", false);
                }
               
            }
        });
    }
    
}
function desactivar(idtecnico){

	bootbox.confirm("Esta seguro que quiere inhabilitar al tecnico?", function(result){
            
		if(result){                    
            $.post("../ajax/tecnicoAjax.php?exe=desactivar",{idtecnico}, function(e){
                bootbox.alert(e);
                mostarform(false);
                tablaUser.ajax.reload(null, true);
                desactive();
            });	
		}
	});
}
function volver() {
    mostarform(false)
  }
init()