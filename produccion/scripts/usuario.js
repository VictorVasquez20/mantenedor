var tabla;
function init (){
    getListuser()
    mostarform(false)
    rutformat()
    desactive()
    getSupervisor()
    getRol()
    getCategoTec()
    $('#divSupervisor').hide()
    $('#divCargo').hide()
    $("#btnGuardar").on("click", function(e){
		submitUser(e);
    });
    $('#div_check').hide()
    $('#ckpass').on('change',function(){
        if($(this).is(':checked')){
            $("#password").removeAttr('disabled');
        }else{
            $('#password').prop('disabled', true);
        }
    })
    $("#tipo_documento").on("change", function(e){
        $("#num_documento").removeAttr('disabled');
        $("#num_documento").attr('readonly', false);            
        $("#num_documento").inputmask('remove');
        if(this.value=='RUT'){                    
                    $("#num_documento").inputmask({"mask": "99.999.999-*"}); //specifying options                                
                }else if(this.value=='P'){
                    $("#num_documento").inputmask({"mask": "P-*{1,40}"}); //specifying options           
        }
    });
}
function getCategoTec(){
    $.post('../ajax/usuarioAjax.php?exe=getCategoTec',function(e){
        $('#idcategoria').html(e);
        $("#idcategoria").selectpicker('refresh');
    })
}

function hideSelected(){
    let values = $('#idrol').val()
  if(values === '1' || values === '4' || values === '6' || values === '7' || values === '9' || values === '11' || values === '13' || values === '16' || values === '19'){
    $('#divSupervisor').show()
    $('#divCargo').show()
  }else{
    $('#divSupervisor').hide()
    $('#divCargo').hide()
  }
}
function rutformat(){
    $("#num_documento").focusout(function () {
        var regexp = /^\d{2}.\d{3}.\d{3}-[k|K|\d]{1}$/;

            if( ( $("#tipo_documento").val()== 'RUT' && regexp.test($("#num_documento").val()) ) ||
                 ( $("#tipo_documento").val()!= 'RUT' && $("#num_documento").val().length > 0 ) ){  
                    
                    $.post("../ajax/usuarioAjax.php?exe=validarExisteNumDocumento",{"num_documento": $("#num_documento").val()}, function(data){
                        let { datos, userData, tecData } = JSON.parse(data)
                        if(parseInt(datos.cantidad)> 0){
                            if(userData.estado === "0"){
                                bootbox.confirm("Usuario Desactivado, Desa activarlo?", function(result){
                                    if(result){  
                                        active()
                                        $("#iduser").val(userData.iduser);	
                                        $("#nombre").val(userData.nombre);
                                        $("#apellido").val(userData.apellido);
                                        $("#username").val(userData.username); 
                                        $("#password").val(userData.password); 
                                        $("#fecha_nac").val(userData.fecha_nac);
                                        $("#direccion").val(userData.direccion);
                                        $("#movil").val(userData.telefono);	
                                        $("#email_corporativo").val(userData.email); 
                                        $("#password_v").val(userData.password);
                                        $("#idrol").val(userData.idrole).trigger('change');
                                        $("#idrol").selectpicker('refresh')
                                        $("#btnGuardar").text('Activar');
                                        $("#btnGuardar").prop("disabled", false);
                                        if(tecData !== undefined){
                                            $("#idtecnico").val(tecData.idtecnico);
                                            $("#idsupervisor").val(tecData.idsupervisor).trigger('change');
                                            $("#idcategoria").val(tecData.idcargotec).trigger('change');
                                            $("#idsupervisor").selectpicker('refresh')
                                            $("#idcategoria").selectpicker('refresh')
                                        }
                                        editarHide(true);
                                        active();
                                    }else{
                                        $("#num_documento").val('')
                                        $("#password").val(''); 
                                    }
                                })
                                
                                /*$("#btnGuardar").prop("disabled", false);
                                new PNotify({
                                    title: 'Advertencia!',
                                    text: ' Usuario Desactivado.',
                                    type: 'warning',
                                    styling: 'bootstrap3'
                                }); */
                            }else{
                                $("#btnGuardar").text('Agregar');
                                desactive()
                                $("#btnGuardar").prop("disabled", true);
                                new PNotify({
                                    title: 'Error!',
                                    text: 'Usuario ya existe.',
                                    type: 'error',
                                    styling: 'bootstrap3'
                                }); 
                            }

                             
                        }else{
                            $("#btnGuardar").text('Agregar');
                            active();
                            $("#btnGuardar").prop("disabled", false);
                            new PNotify({
                                title: 'Success!',
                                text: 'Ingrese datos del usuario.',
                                type: 'success',
                                styling: 'bootstrap3'
                            }); 
                        }
                    })
           }else{
            
                $("#btnGuardar").prop("disabled", true);
                desactive()
                new PNotify({
                    title: 'Error!',
                    text: 'Debe completar el numero.',
                    type: 'error',
                    styling: 'bootstrap3'
                });  
               
           }
            let corte =  $("#num_documento").val().split('.')
            let cortedv = corte[2].split('-');
            let pass = corte[1].substr(2) + cortedv[0]
            $("#password").val(pass); 
            $("#password_v").val(pass); 
   });
}
function getListuser(){
    tablaUser = $('#tablaUser').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/usuarioAjax.php?exe=getListUser',
            type: "POST",
            dataType: "json",
            error: function (e) {
                $('#tablaUser').hide();
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "asc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}
function mostarform(flag){
    $("#btnGuardar").text('Agregar');
    limpiar();
	if(flag){
        //editarHide(false)
        desactive()
		$("#listadousuarios").hide();
		$("#formulariousuario").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", true);

	}else{
        desactive()
        //editarHide(false)
		$("#listadousuarios").show();
		$("#formulariousuario").hide();
		$("#op_agregar").show();
        $("#op_listar").hide();
        $("#btnGuardar").prop("disabled", true)
        //$("#FichaEmpleado").hide();
	}

}
function limpiar(){
  
	$("#iduser").val("");	
	$("#nombre").val("");
    $("#apellido").val("");
    $("#nombre_v").val("");
	$("#apellido_v").val("");
	$("#tipo_documento").val("");
	$("#tipo_documento").selectpicker('refresh');
	$("#num_documento").val("");
    $("#num_documento").inputmask('remove');
    $("#num_documento").inputmask({"mask": "99.999.999-*"}); //specifying options
    $("#num_documento").attr('readonly', true);
	$("#fecha_nac").val("");
	$("#direccion").val("");
	$("#movil").val("");	
    $("#email_corporativo").val("");
    $("#username").val(""); 
    $("#password").val(""); 
    $("#password_v").val(""); 
    $("#idsupervisor").val(""); 
    $("#idrol").val(""); 
    $("#idcategoria").val(""); 

    $("#idsupervisor").selectpicker('refresh')
    $("#idrol").selectpicker('refresh')
    $("#idcategoria").selectpicker('refresh')

}
function desactive(){
    $("#nombre").prop('disabled', true);
    $("#apellido").prop('disabled', true);
    $("#username").prop('disabled', true);
    //$("#num_documento").prop('disabled', true);
    $("#fecha_nac").prop('disabled', true);
    $("#direccion").prop('disabled', true);
    $("#idsupervisor").prop('disabled', true)
    $("#movil").prop('disabled', true);
    $('#email_corporativo').prop('disabled', true);
    $('#idrol').prop('disabled', true);
    $('#password').prop('disabled', true);
    $('#idcategoria').prop('disabled', true);
    $('#div_check').hide()
 
}
function active(){
    $("#nombre").removeAttr('disabled');
    $("#apellido").removeAttr('disabled');
    $("#username").removeAttr('disabled');
    $("#fecha_nac").removeAttr('disabled');
    $("#direccion").removeAttr('disabled');
    $("#movil").removeAttr('disabled');
    $('#email_corporativo').removeAttr('disabled');
    $('#idrol').removeAttr('disabled');
    $('#idcategoria').removeAttr('disabled');
    //$("#password").removeAttr('disabled');
    $('#div_check').show();
    $("#idsupervisor").removeAttr('disabled');
    $("#idsupervisor").selectpicker('refresh')
    $("#idrol").selectpicker('refresh')
    $("#idcategoria").selectpicker('refresh')

}
function editarHide(edit){
    if(edit){
        $('.spanDirec').show();
        $("#direccion").get(0).type = 'text';    
        $("#movil").get(0).type = 'text';  
        $("#email_corporativo").get(0).type = 'email';  
        $("#fecha_nac").get(0).type = 'date';  

    }else{
        $('.spanDirec').hide();
        $("#direccion").get(0).type = 'hidden';    
        $("#movil").get(0).type = 'hidden';  
        $("#email_corporativo").get(0).type = 'hidden';  
        $("#fecha_nac").get(0).type = 'hidden';  
    }
}
function getSupervisor(){
    $.post('../ajax/tecnicoAjax.php?exe=getSupervisor',function(e){
        $('#idsupervisor').html(e);
        $("#idsupervisor").selectpicker('refresh');
    })
}

function submitUser(e){
    e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    
    $.ajax({
		url:'../ajax/usuarioAjax.php?exe=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
            if(datos !== 'NOMBRE DE USUARIO EXISTENTE')
            {
                if($("#btnGuardar").text() === 'Activar'){
                    bootbox.alert('Usuario Activado');
                }else{
                    bootbox.alert(datos);
                }
                mostarform(false);
                tablaUser.ajax.reload(null, true);
                desactive();
                //tabla.ajax.reload();
            }else{
                bootbox.alert(datos);
                $("#btnGuardar").prop("disabled", false);
            }
           
        }
    });
}
function mostrar(iduser){
    $.post("../ajax/usuarioAjax.php?exe=mostrar",{iduser}, function(data){
        data = JSON.parse(data);
        mostarform(true);
        active()
		$("#iduser").val(data.iduser);	
		$("#nombre").val(data.nombre);
        $("#apellido").val(data.apellido);
        $("#username").val(data.username); 
        $("#password").val(data.password); 
        $("#password_v").val(data.password);
        $("#idtecnico").val(data.idtecnico);
        $("#idsupervisor").val(data.idsupervisor).trigger('change');
        $("#idrol").val(data.idrol).trigger('change');
        $("#idcategoria").val(data.idcategoria).trigger('change');
        $("#idsupervisor").selectpicker('refresh')
        $("#idrol").selectpicker('refresh')
        $("#idcategoria").selectpicker('refresh')
		$("#tipo_documento").val(data.tipo_documento).trigger('change');
		$("#tipo_documento").selectpicker('refresh');                
            $("#num_documento").removeAttr("data-inputmask");
            $("#num_documento").inputmask('remove');
		if($("#tipo_documento").val()=='RUT'){                    
            $("#num_documento").inputmask({"mask": "99.999.999-*"}); //specifying options                                
        }else if($("#tipo_documento").val()=='P'){
            $("#num_documento").inputmask({"mask": "P-*{1,40}"}); //specifying options           
        }
		$("#num_documento").val(data.num_documento);
		$("#fecha_nac").val(data.fecha_nac);
		$("#direccion").val(data.direccion);
		$("#movil").val(data.movil);	
        $("#email_corporativo").val(data.email_corporativo); 
        editarHide(true);
        active();
        $("#btnGuardar").prop("disabled", false);
	});
}
function desactivar(iduser){
 	bootbox.confirm("Esta seguro que quiere inhabilitar el usuario?", function(result){
            
		if(result){                    
            $.post("../ajax/usuarioAjax.php?exe=desactivar",{iduser}, function(e){
                bootbox.alert(e);
                mostarform(false);
                tablaUser.ajax.reload(null, true);
                desactive();
            });	
		}
	});
}
function volver() {
  mostarform(false)
}

function getRol(){
    $.post('../ajax/usuarioAjax.php?exe=getRole',function(e){
        $('#idrol').html(e);
        $("#idrol").selectpicker('refresh');
    })
}

init()