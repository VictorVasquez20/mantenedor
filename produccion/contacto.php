<?php

require 'header.php';

if (!isset($_SESSION["nombre"])) {
	header("Location:login.php");
} else {
?>

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contactos GSE</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                                    </li>
                                    <li><a id="op_listar" onclick="mostarform(false)">LISTAR</a>
                                    </li>
                                </ul>
                            </li>               
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                    <div id="listadocontacto">
                        <table id="tablaContacto" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>OPCIONES</th>
                                    <th>NOMBRE</th>
                                    <th>CARGO</th>
                                    <th>EMAIL</th>
                                    <th>NOMBRE EDIFICIO</th>
                                    <th>TELEFONO MOVIL</th>
                                    <th>TELEFONO FIJO</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>                       
                    </div>
                    <div id="formulariocontacto" class="x_content">
                      <form id="formulario" name="formulario"  class="form-horizontal form-label-left input_mask">
                        <div class="x_title col-md-12 col-sm-12 col-xs-12">
                          <h4>Buscador</h4>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-10 form-group has-feedback">
                          <input type="text" class="form-control has-feedback-left" id="buscar" name="buscar" placeholder="Buscar Edificios" required="Campo requerido" >
                          <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                        </div>
                       
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback ">
                          <button class="btn btn-success" type="submit" id="btnBuscar">Buscar</button>
                        </div>
                        <div id='buscadorDiv' class="x_title col-md-12 col-sm-12 col-xs-12">
                        
                        </div>
                        </form>
                        <form id="formularioCrear">
                          <div id='crearDiv' class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 30px;">
                            <div class="col-md-10 col-sm-10 col-xs-10 form-group has-feedback">
                              <input type="number" class="form-control has-feedback-left" id="count" name="count" placeholder="Cantidad de usuario a agregar" required="Campo requerido" >
                              <span class="fa fa-users spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback ">
                             <button class="btn btn-success" type="submit" id="btncrearCont">Crear Contacto</button>
                            </div>
                          </div>
                        </form>
                        <form id="formularioGuardar">
                          <div id='crearContacto' class="col-md-12 col-sm-12 col-xs-12">
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                               <button class="btn btn-success" type="submit" id="btnGuardarContacto">Agregar</button>
                          </div>
                        </form>
                    </div>
                    <div id='divEdit'>
                      <form id='formularioEdit'>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                          <label>Nombre</label>
                          <input type="hidden" id="idEdit" name="idEdit" class="form-control has-feedback-left">
                          <input type="text" class="form-control has-feedback-left" id="nombreEdit" name="nombreEdit" placeholder="Ingresar nombre" required="Campo requerido">
                          <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class='col-md-6 col-sm-6 col-xs-6 form-group has-feedback'>
                          <label>Cargo</label>
                          <input type="text" class="form-control has-feedback-left" id="cargoEdit" name="cargoEdit" placeholder="Ingresar cargo" required="Campo requerido">
                          <span class="fa fa-building-o spn form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class='col-md-4 col-sm-4 col-xs-4 form-group has-feedback'>
                          <label>Correo</label>
                          <input type="email" class="form-control has-feedback-left" id="emailEdit" name="emailEdit" placeholder="Ingresar correo" required="Campo requerido">
                          <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class='col-md-4 col-sm-4 col-xs-4 form-group has-feedback'>
                          <label>Teléfono Móvil</label>
                          <input type="text" class="form-control has-feedback-left" id="telMovilEdit" name="telMovilEdit" placeholder="Ingresar teléfono móvil" data-inputmask="'mask' : '+56(9)9999-9999'" >
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class='col-md-4 col-sm-4 col-xs-4 form-group has-feedback'>
                          <label>Teléfono Fijo</label>
                          <input type="text" class="form-control has-feedback-left" id="telFijoEdit" name="telFijoEdit" placeholder="Ingresar teléfono fijo" data-inputmask="'mask' : '+56(2)9999-9999'">
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="volver()">Volver</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiarEdit()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnEditarContacto">Editar</button>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php 
    require 'footer.php';
?>
<?php
    echo '<script type=text/javascript src="scripts/contacto.js?'.$_SESSION["version"].'"></script>';
?>
   
    <?php
}

ob_end_flush();