<?php

require 'header.php';

if (!isset($_SESSION["nombre"])) {
	header("Location:login.php");
} else {
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tecnicos GSE</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-tooltip="tooltip" title="Operaciones" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a id="op_agregar" onclick="mostarform(true)">Agregar</a>
                                    </li>
                                    <li><a id="op_listar" onclick="mostarform(false)">LISTAR</a>
                                    </li>
                                </ul>
                            </li>               
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                    <div id="listadotecnico">
                        <table id="tablatecnico" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>OPCIONES</th>
                                    <th>NOMBRE Y APELLIDO</th>
                                    <th>RUT</th>
                                    <th>TELEFONO</th>
                                    <th>EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="formulariotecnico" class="x_content">
                        <br />
                        <form id="formulario" name="formulario"  class="form-horizontal form-label-left input_mask">
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <label>NUMERO DE DOCUMENTO</label>  
                                <input type="text" class="form-control has-feedback-left" id="num_documento" name="num_documento" placeholder="Numero de Documento" required="required"  maxlength="40">
                                <span class="fa fa-id-card form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <label>Supervisor</label>  
                                <select class="form-control selectpicker" data-live-search="true" id="idsupervisor" name="idsupervisor" required="required" disabled>
                                <option value="" selected></option>
                                </select>
							</div>     
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="hidden" class="form-control has-feedback-left" id="idtecnico" name="idtecnico">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Nombre</label>
                                <input type="text" class="form-control has-feedback-left" id="nombre" name="nombre" placeholder="Nombres" required="Campo requerido" >
                                <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Apellido</label>
                            <input type="text" class="form-control has-feedback-left" id="apellido" name="apellido" placeholder="Apellidos" required="Campo requerido">
                            <span class="fa fa-user spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label>Contacto</label>
                                <input type="text" id="movil" name="movil" class="form-control has-feedback-left" placeholder="Telefono Movil" data-inputmask="'mask' : '+56(9)9999-9999'" required="Campo requerido">
                                <span class="fa fa-mobile spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                            <label>Correo</label>
                                <input type="text" class="form-control has-feedback-left" id="email_corporativo" style="text-transform: lowercase" name="email_corporativo" placeholder="Email Corporativo" required="Campo requerido">
                                <span class="fa fa-envelope-o  spn form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <label>Cargo</label>  
                                <select class="form-control selectpicker" data-live-search="true" id="idcargotec" name="idcargotec" required="required" disabled>
                                <option value="" selected></option>
                                </select>
							</div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="volver()">Volver</button>
                                  <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                  <button class="btn btn-success" type="submit" id="btnGuardar">Editar</button>
                                </div>
                            </div>
                        </form>
                        </div>
                </div>
            </div>
        </div>                   
    </div>
</div>
<?php 
    require 'footer.php';
?>
<?php
    echo '<script type=text/javascript src="scripts/tecnico.js?'.$_SESSION["version"].'"></script>';
?>
   
    <?php
}

ob_end_flush();