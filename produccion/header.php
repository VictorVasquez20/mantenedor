<?php
ob_start();
session_start();
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MANTENEDOR APPFABRIMETAL</title>

    <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />
    
    <!-- Bootstrap -->
    <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../public/build/css/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../public/build/css/green.css" rel="stylesheet">

    <!-- Datatables -->
    <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- FORMS -->
    <link href="../public/build/css/prettify.min.css" rel="stylesheet">
    <link href="../public/build/css/select2.min.css" rel="stylesheet">
    <link href="../public/build/css/switchery.min.css" rel="stylesheet">
    <link href="../public/build/css/starrr.css" rel="stylesheet">

    <!-- bootstrap-file-imput -->
    <link href="../public/build/css/fileinput.min.css" rel="stylesheet">

    <!-- PNotify -->
    <link href="../public/build/css/pnotify.css" rel="stylesheet">
    <link href="../public/build/css/pnotify.buttons.css" rel="stylesheet">
    <link href="../public/build/css/pnotify.nonblock.css" rel="stylesheet">
    
    <!-- bootstrap-select -->
    <link href="../public/build/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="../public/build/css/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../public/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="../public/build/css/normalize.css" rel="stylesheet">
    <link href="../public/build/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="../public/build/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="../public/build/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../public/build/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../public/build/css/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../public/build/css/daterangepicker.css" rel="stylesheet">

        <!-- FullCalendar -->
    <link href="../public/build/css/fullcalendar.min.css" rel="stylesheet">
    <link href="../public/build/css/fullcalendar.print.css" rel="stylesheet" media="print">

    <!-- Multi select-->
    <link href="../public/build/css/multi-select/multi-select.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../public/build/css/custom.css" rel="stylesheet">
    
    <!-- Sweet -->
    <link rel="stylesheet" href="../public/sweet/dist/sweetalert2.min.css">

    <style>
      *{
        text-transform: uppercase;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a href="usuarios.php" class="site_title"><img src="../files/logo.png" height="40" width="200"> <span>Fabrimetal</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../files/usuarios/<?=$_SESSION['imagen'] ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h5 style="text-transform:none;"><?=$_SESSION['nombre']." ".$_SESSION['apellido']?></h5>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                  <h3>Fabrimetal</h3>
                  <ul class="nav side-menu">
                  <li><a href="usuarios.php"><i class="fa fa-paper-plane"></i>Usuarios APP FAB</a></li>
                  <li><a href="tecnico.php"><i class="fa fa-paper-plane"></i>Tecnicos GSE</a></li>
                  <li><a href="contacto.php"><i class="fa fa-paper-plane"></i>Contactos GSE</a></li>
                  <li><a href="modificacionGSE.php"><i class="fa fa-paper-plane"></i>Modificación GSE</a></li>
                  </ul>
              </div>               
            </div>
          <!-- /sidebar menu -->

           <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!--<?php
               echo '<a data-toggle="tooltip" data-placement="top" title="Manual de usuario" href="../files/documentacion/Manual_Bitacora_Colaborador.pdf?' . $_SESSION["version"] . '" target="_blank">'
              ?>
              </a>-->
              <!--<?php
              echo '<a data-toggle="tooltip" data-placement="top" title="Historial de mejoras" href="../files/documentacion/Cambios_AppBitacora.pdf?' . $_SESSION["version"] . '" target="_blank">'
              ?>
              </a>-->
              <a data-toggle="tooltip" data-placement="top" title="Pantalla completa" onclick="fullsecreen()">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
                <a data-toggle="tooltip" data-placement="top" title="Salir" href="../ajax/login.php?exe=logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
          </div>
          <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="text-transform:none;">
                    <img src="../files/usuarios/<?=$_SESSION['imagen'] ?>" alt=""><?=$_SESSION['nombre']." ".$_SESSION['apellido'] ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                      <li><a href="../ajax/login.php?exe=logout"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        
        