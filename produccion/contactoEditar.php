<?php

require 'header.php';

if (!isset($_SESSION["nombre"])) {
	header("Location:login.php");
} else {
?>

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contacto</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                    <div id="formulariocontacto" class="x_content">
                      <form id="formulario" name="formulario"  class="form-horizontal form-label-left input_mask">
                        
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php 
    require 'footer.php';
?>
<?php
    echo '<script type=text/javascript src="scripts/contacto.js?'.$_SESSION["version"].'"></script>';
?>
   
    <?php
}

ob_end_flush();