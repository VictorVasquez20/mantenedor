<?php

require 'header.php';

if (!isset($_SESSION["nombre"])) {
	header("Location:login.php");
} else {
    ?>
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Modificación GSE</h2>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                    <div id="filtroguias" class="x_content">
                                <div class="form-group">
                                  <form id="formularioBuscar" name="formularioBuscar"  class="form-horizontal form-label-left input_mask">
                                    <div class="col-md-4">
                                        <label>MES</label>
                                        <select id="mes" name="mes" class="selectpicker form-control" required="required">
                                            <option value="" selected disabled>SELECCIONE MES</option>
                                            <option value="1">ENERO</option>
                                            <option value="2">FEBRERO</option>
                                            <option value="3">MARZO</option>
                                            <option value="4">ABRIL</option>
                                            <option value="5">MAYO</option>
                                            <option value="6">JUNIO</option>
                                            <option value="7">JULIO</option>
                                            <option value="8">AGOSTO</option>
                                            <option value="9">SEPTIEMBRE</option>
                                            <option value="10">OCTUBRE</option>
                                            <option value="11">NOVIEMBRE</option>
                                            <option value="12">DICIEMBRE</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>AÑO</label>
                                        <select id="ano" name="ano" class="selectpicker form-control" required="required">
                                          <option value="" selected disabled>SELECCIONE AÑO</option>
                                          <option value="2021">2021</option>
                                          <option value="2020">2020</option>
                                          <option value="2019">2019</option>
                                          <option value="2018">2018</option>
                                          <option value="2017">2017</option>
                                          <option value="2016">2016</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label>GSE</label>
                                        <input type="text" class="form-control" id="gse" name="gse" placeholder="INGRESE GSE">
                                    </div>
                                    <div class="col-md-2">
                                        <br>
                                        <button type="submit" class="btn btn-info">BUSCAR</button>
                                    </div>
                                  </form>
                                </div>
                    </div>
                    <div id="listadoGuia">
                        <table id="tablaGuia" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>OPCIONES</th>
                                    <th>SERVICIO</th>
                                    <th>NOMBRE EDIFICIO</th>
                                    <th>CODIGO FM</th>
                                    <th>NOMBRE TECNICO</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="editarGuia">
                    <label style="font-size:20px">SERVICIO N° </label><label id="idserviciol" style="font-size:20px"></label></br></br></br>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                      </div>
                      <form id="formulario" name="formulario"  class="form-horizontal form-label-left input_mask">
                         <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                         <input type="hidden" class="form-control has-feedback-left" id="idservicio" name="idservicio">
                           <label>Codigo FM</label>  
                           <select class="form-control selectpicker" data-live-search="true" id="codigoFm" name="codigoFm" required="required">
                             <option value="" selected></option>
                           </select>
					     </div>  
                         <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                           <label>Tipo Servicio</label>  
                           <select class="form-control selectpicker" data-live-search="true" id="tipoServicio" name="tipoServicio" required="required">
                             <option value="" selected></option>
                           </select>
					     </div>
                         <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                           <label>Estado Inicial</label>  
                           <select class="form-control selectpicker" data-live-search="true" id="estadoInicio" name="estadoInicio" required="required">
                             <option value="" selected></option>
                           </select>
					     </div>
                         <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                           <label>Estado Final</label>  
                           <select class="form-control selectpicker" data-live-search="true" id="estadoFin" name="estadoFin" required="required">
                             <option value="" selected></option>
                           </select>
					     </div>
                         <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                           <label>Observación al inicial</label>  
                           <textarea type="text" id="obsInicio" name="obsInicio" style="resize:none; width:100%; height:18rem;" class="form-control" required="required"></textarea>
					     </div>
                         <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                           <label>Observación al finalizar</label>  
                           <textarea type="text" id="obsFin" name="obsFin" style="resize:none; width:100%;height:18rem;" class="form-control" required="required"></textarea>
					     </div>
                         <div class="form-group">
                           <div class="col-md-6 col-sm-6 col-xs-12">
                               <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="volver()">Volver</button>
                               <button class="btn btn-success" type="submit" id="btnEditar">Agregar</button>
                           </div>
                         </div>
                      </form>
                    </div>
                   
                </div>
            </div>
        </div>                   
    </div>
</div>
<?php 
    require 'footer.php';
?>
<?php
    echo '<script type=text/javascript src="scripts/modificacionGSE.js?'.$_SESSION["version"].'"></script>';
?>
   
    <?php
}

ob_end_flush();