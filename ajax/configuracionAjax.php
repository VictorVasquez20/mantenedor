<?php
function crearLog(){
    $archivoLog = fopen("../modelos/Log.php","w") or die("No se puede abrir/crear el archivo!");
    $username = '$username';
    $idempleado = '$idempleado';
    $password = '$password';
    $php = '
    <?php 
    require_once "../config/conexion.php";
    Class Log{
        private $idlog;
        private $fecha_actualizacion;
        private $idempleado;
        private $username;
        private $proceso;
        private $codigo;
        
        public function __construct(){
    
        }
        private function list($username = false, $codigo = false){
            $sql = "SELECT * FROM log";
            if($username){
                $sql .=" WHERE username = '."'$username'".'";
            }
            if($codigo){
                $sql .= " AND codigo = $codigo";
            }
            return db_query($sql);
        }
        private function insert($idempleado,$username,$password,$codigo){
            $password = hash('."'SHA256'".', $password);
            $sql = "INSERT INTO log (idempleado,codigo,username,proceso) VALUES ('."'$idempleado'".',$codigo,'."'$username'".','."'$password'".')";
            return ejecutarConsulta($sql);
        }
    
        public function insertLog($idempleado,$username,$password,$codigo){
            return $this->insert($idempleado,$username,$password,$codigo);
        }
        public function listLogCount($username,$codigo){
            return $this->list($username,$codigo);
        }
        /**
         * Get the value of idlog
         */ 
        public function getIdlog()
        {
            return $this->idlog;
        }
    
        /**
         * Set the value of idlog
         *
         * @return  self
         */ 
        public function setIdlog($idlog)
        {
            $this->idlog = $idlog;
    
            return $this;
        }
    
        /**
         * Get the value of fecha_actualizacion
         */ 
        public function getFecha_actualizacion()
        {
            return $this->fecha_actualizacion;
        }
    
        /**
         * Set the value of fecha_actualizacion
         *
         * @return  self
         */ 
        public function setFecha_actualizacion($fecha_actualizacion)
        {
            $this->fecha_actualizacion = $fecha_actualizacion;
    
            return $this;
        }
    
        /**
         * Get the value of idempleado
         */ 
        public function getIdempleado()
        {
            return $this->idempleado;
        }
    
        /**
         * Set the value of idempleado
         *
         * @return  self
         */ 
        public function setIdempleado($idempleado)
        {
            $this->idempleado = $idempleado;
    
            return $this;
        }
        public function getUsername()
        {
            return $this->username;
        }
    
        public function setUsernameo($username)
        {
            $this->username = $username;
    
            return $this;
        }
        public function getProceso()
        {
            return $this->proceso;
        }
    
       
        public function setProceso($proceso)
        {
            $this->proceso = $proceso;
    
            return $this;
        }
        public function getCodigo()
        {
            return $this->codigo;
        }
    
       
        public function setCodigo($codigo)
        {
            $this->codigo = $codigo;
    
            return $this;
        }
    }    
    ?>
    
    ';
    fwrite($archivoLog, $php);
    fclose($archivoLog);
}
function crearEvento(){
    $archivoLog = fopen("../modelos/Evento.php","w") or die("No se puede abrir/crear el archivo!");
    $nombreEvento = '$nombreEvento';
    $php = '
    <?php 
    require_once "../config/conexion.php";
    Class Evento{
        private $idevento;
        private $descripcion;
        private $activo;
        private $codigo;

        public function __construct(){
    
        }
        private function list($activo = 1, $codigo = false){
            $sql = "SELECT * FROM evento where activo = $activo";
            if($codigo){
                $sql .= " AND codigo = $codigo";
            }

            return db_query($sql);
        }
        private function insert($nombreEvento, $codigo){
            $sql = "INSERT INTO evento (descripcion,codigo) VALUES ('."'$nombreEvento'".',$codigo)";
            return ejecutarConsulta($sql);
        }
        private function updateActive ($idevento){
            $sql = "UPDATE evento SET activo = 0 WHERE idevento = $idevento";
            return ejecutarConsulta($sql);
        }
        public function inactivarEvento($idevento){
            return $this->updateActive($idevento); 
        }
    
        public function insertEvento($nombreEvento,$codigo){
            return $this->insert($nombreEvento,$codigo);
        }
        public function listEvento(){
            return $this->list();
        }
       
        public function getIdEvento()
        {
            return $this->idevento;
        }
    
       
        public function setIdevento($idevento)
        {
            $this->idevento = $idevento;
    
            return $this;
        }
    
        public function getDescripcion()
        {
            return $this->descripcion;
        }
    
       
        public function setDescripcion($descripcion)
        {
            $this->descripcion = $descripcion;
    
            return $this;
        }

        public function getActivo()
        {
            return $this->activo;
        }
    
       
        public function setActivo($activo)
        {
            $this->activo = $activo;
    
            return $this;
        }

        public function getCodigo()
        {
            return $this->codigo;
        }
    
       
        public function setCodigo($codigo)
        {
            $this->codigo = $codigo;
    
            return $this;
        }
    }    
    ?>';
    fwrite($archivoLog, $php);
    fclose($archivoLog);
}
function crearAjaxEvento(){
    $archivoLog = fopen("eventoAjax.php","w") or die("No se puede abrir/crear el archivo!");
    $row = '$row';
    $button = '<button class="btn btn-danger btn-xs" id="incativarEvento" value="'.$row['idevento'].'"><i class="fa fa-times"></i></button>';
    $php ='
    <?php
    session_start();
    include "../modelos/Evento.php";
    
    $evento = new Evento();
    
    function tableList(){
        global $evento;
        $query = $evento->listEvento();
        $data = Array();
        while ($row = db_fetch($query)){
            $data["aaData"][] = array(
                "0" => '.$button.',
                "1" => $row["descripcion"],
                "2" => $row["codigo"]
            );
        }
        //return var_dump($query);
        return json_encode($data);
    }
    
    function guardarEvento(){
        global $evento;
        $nombreEvento = $_POST["nombreEvento"];
        $codigo = $_POST["codigo"];
        $query = $evento->insertEvento($nombreEvento,$codigo);
        if($query === TRUE){
            echo "EVENTO REGISTADO CORRECTAMENTE";
        }else{
            echo "Error Al CREAR EL REGISTRO";
        }
    }
    function inactivarEvento(){
        global $evento;
        $idevento = $_POST["idevento"];
        $query = $evento->inactivarEvento($idevento);
        if($query === TRUE){
            echo "EVENTO REGISTADO CORRECTAMENTE";
        }else{
            echo "Error Inactivar EL REGISTRO";
        }
    }
    if (isset($_GET["exe"]) AND $_GET["exe"] != "") {
        echo $_GET["exe"]();
    }
    ?>';

    fwrite($archivoLog, $php);
    fclose($archivoLog);
}
function createTable(){
    $nombreDb = $_POST['nombreDb'];
    $ipServer = $_POST['ipServer'];
    $userDb = $_POST['userDb'];
    $passDb = $_POST['passDb'];
    $conn = new mysqli($ipServer, $userDb, $passDb, $nombreDb);
    if($conn->connect_error){
        die('Conección Fallida'.$conn->connect_error);
    }

    /// DROP TABLE
    $sql="DROP TABLE IF EXISTS log, evento";
    $conn->query($sql);

    ///TABLA LOG
    $sql="CREATE TABLE log(
        idlog INT  AUTO_INCREMENT PRIMARY KEY,
        idempleado INT NULL,
        codigo INT NULL,
        username VARCHAR(50) NULL,
        proceso VARCHAR(200) NULL,
        fecha_actualizacion DATE DEFAULT CURRENT_TIMESTAMP
    )";
    if ($conn->query($sql) === TRUE) {
        echo "Tabla Creada con Exito";
    } else {
        echo "Error Al Crear Tabla: " . $conn->error;
    }
    /// TABLA EVENTO
    $sql="CREATE TABLE evento(
        idevento INT  AUTO_INCREMENT PRIMARY KEY,
        descripcion VARCHAR(50) NULL,
        activo INT DEFAULT 1,
        codigo INT NULL
    )";
    if ($conn->query($sql) !== TRUE) {
        echo "Error Al Crear Tabla: " . $conn->error;
    } 
   
    $conn->close();
    crearLog();
    crearEvento();
    crearAjaxEvento();
}


if (isset($_GET['exe']) AND $_GET['exe'] != '') {
	echo $_GET['exe']();
}

?>