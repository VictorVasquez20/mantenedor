<?php
session_start();
require_once "../config/conexion.php";
$tecnico = new Tecnico();
$supervisor = new Supervisor();
$cargotec = new Cargotec();
function getListTecnico(){
    global $tecnico;

   $query = $tecnico->list();
   $data = Array();
	while ($row = db_fetch($query)){
		$data['aaData'][] = array(
			"0"=>'<button class="btn btn-danger btn-xs" onclick="desactivar('.$row['idtecnico'].')" title="desactivar empleado"><i class="fa fa-close"></i></button>',                       
			"1"=>$row['nombre'].' '.$row['apellidos'],
			"2"=>$row['rut'],
			"3"=>$row['telefono_interno'],
			"4"=>$row['email_interno']
	);
	}
    echo json_encode($data);
}
function getSupervisor(){
	global $supervisor;
	$query = $supervisor->list();
	echo '<option value="" selected disabled>SELECCIONE SUPERVISOR</option>';
    while($row = db_fetch($query)){
        echo '<option value='.$row['idsupervisor'].'>'.$row['nombre'].'  '.$row['apellido'].'</option>';
    }
}
function getCategoTec(){
	global $cargotec;
	$query = $cargotec->list();
	echo '<option value="" selected disabled>SELECCIONE CARGO</option>';
    while($row = db_fetch($query)){
        if($row['nombre'] != 'PRUEBA'){
            echo '<option value='.$row['idcargotec'].'>'.$row['nombre'].'</option>';
        }
    }
}
function guardar(){
	global $tecnico;
	$nombre=strtoupper($_POST["nombre"]);
    $apellido=strtoupper($_POST["apellido"]);
    $direccion=strtoupper($_POST["direccion"]);
    $movil=$_POST["movil"];
    $email_corporativo=strtolower($_POST["email_corporativo"]);
    $num_documento = $_POST["num_documento"];
    $idSupervisor = $_POST["idsupervisor"];
	$idcategoria = $_POST["idcargotec"];
	if($tecnico->insertTecnico($idcategoria, $idSupervisor, $nombre, $apellido, $num_documento, $movil, $email_corporativo, 1)){
		return 'TECNICO REGISTRADO';
	}else{
		return 'ERROR AL INSERTAR';
	}
}
function desactivar(){
    global $tecnico;
    $idtecnico = $_POST["idtecnico"];
    if($tecnico->desactivar($idtecnico)){
        return "Empleado inhabilitado";
    }else{
        return "Empleado no se pudo inhabilitar";
    }
}
function activar(){
    global $tecnico;
    $idtecnico = $_POST["idtecnico"];
    if($tecnico->activar($idtecnico)){
        return "TÉCNICO Activado";
    }else{
        return "TÉCNICO no se pudo Activar";
    }
}

if (isset($_GET['exe']) AND $_GET['exe'] != '') {
	echo $_GET['exe']();
}