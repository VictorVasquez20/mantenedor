<?php
session_start();
require_once "../config/conexion.php";
require_once "../config/conexionSap.php";
unset($User);
$User = new Usuario();

function login()
{
	global $User;
	
	$rspta=$User->list('',strtolower($_POST['user']), $_POST['pass']);
	$fecth = $rspta->fetch_object();
	//echo $rspta;
	if (strtolower($fecth->iduser) == null) {
		$mensaje["cod"] = 220;
		$mensaje["mensaje"] = "Usuario o contraseña incorrecta";
		return json_encode($mensaje);
	}else{
		LoginSAP();
		$idu = $fecth->iduser;

		
		$_SESSION['iduser'] = $fecth->iduser;
		$_SESSION['nombre'] = $fecth->nombre;
		$_SESSION['apellido'] = $fecth->apellido;
		$_SESSION['num_documento'] = $fecth->num_documento;
		$_SESSION['email'] = $fecth->email;
		$_SESSION['imagen'] = $fecth->imagen;
		$_SESSION['password'] = $fecth->password;
		$_SESSION["version"] = 0;
		$mensaje["cod"] = 200;
		
		return json_encode($mensaje);
	}
}


function logout()
{
	unset($_SESSION['user']);

	header('Location: ../produccion/login.php');
}


if (isset($_GET['exe']) AND $_GET['exe'] != '') {
	echo $_GET['exe']();
}