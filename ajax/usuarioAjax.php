<?php
session_start();
require_once "../config/conexion.php";
$user = new Usuario();
$tecnico = new Tecnico();
$role = new Role();
$cargotec = new Cargotec();
function getListUser(){
    global $user;

   $query = $user->getList();
   $data = Array();
	while ($row = db_fetch($query)){
		$data['aaData'][] = array(
			"0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$row['iduser'].')" title="editar usuario"><i class="fa fa-pencil"></i></button>'.
                 '<button class="btn btn-danger btn-xs" onclick="desactivar('.$row['iduser'].')" title="desactivar usuario"><i class="fa fa-close"></i></button>',                       
			"1"=>$row['username'],
			"2"=>$row['nombre'].' '.$row['apellido'],
			"3"=>$row['num_documento'],
			"4"=>$row['email']
	);
	}
    echo json_encode($data);
}

function guardaryeditar(){
    global $user, $tecnico;
    $idempleado=$_POST["iduser"];
    $nombre=strtoupper($_POST["nombre"]);
    $apellido=strtoupper($_POST["apellido"]);
    $username=strtolower($_POST["username"]);
    $fecha_nac=strtoupper($_POST["fecha_nac"]);
    $direccion=strtoupper($_POST["direccion"]);
    $movil=strtoupper($_POST["movil"]);
    $email_corporativo=strtolower($_POST["email_corporativo"]);
    $tipo_documento =$_POST["tipo_documento"];
    $dv = explode('-', $_POST["num_documento"]);
    if($dv[1] == 'k' || $dv[1] == 'K'){
        $_POST["num_documento"]=strtoupper($_POST["num_documento"]);
    }
    $num_documento =$_POST["num_documento"];
    $idSupervisor =$_POST["idsupervisor"];
    $idrol = $_POST["idrol"];
    $idcategoria = $_POST["idcategoria"];
    $password_v = $_POST["password_v"];
    $password = $_POST["password"];
    $idtecnico = $_POST["idtecnico"];

    $data = json_encode($_POST);
    $query = $user->listUsename($username);
    if($password != null){
        $password_v = $_POST["password"];
    }
    if(empty($idempleado)){
        if($query['cantidad'] == 0){
            if($user->insertUser($idrol,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password_v,$tipo_documento,$num_documento,1,1)){
                if ($idrol == 1 || $idrol == 4 || $idrol == 6 || $idrol == 7 || $idrol == 9 || $idrol == 11 || $idrol == 13 || $idrol == 16 || $idrol == 19){
                    $tecnico->insertTecnico($idcategoria, $idSupervisor, $nombre, $apellido, $num_documento, $movil, $email_corporativo, 1);
                    if($idrol == 7 && $idcategoria == 2){
                        $existeSap = $user->consultaEmpleadoSap($num_documento);
                        if(isset($existeSap['value'][0]['EmployeeID'])){
                            $user->updateEmpleadoSap($existeSap['value'][0]['EmployeeID'],$data);
                        }else{
                            $user->insertaEmpleadoSap($data);
                        }
                    }
                }
               return 'USUARIO REGISTRADO';
            }else{
                return 'ERROR AL INSERTAR';
            }
        }else{
            return 'NOMBRE DE USUARIO EXISTENTE';
        }
    }else{
        if(empty($idtecnico)){
            if($user->updateUser($idempleado,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password_v,$tipo_documento,$num_documento,$idrol)){
                if ($idrol == 1 || $idrol == 4 || $idrol == 6 || $idrol == 7 || $idrol == 9 || $idrol == 11 || $idrol == 13 || $idrol == 16 || $idrol == 19){
                    $tecnico->insertTecnico($idcategoria, $idSupervisor, $nombre, $apellido, $num_documento, $movil, $email_corporativo, 1);
                    if($idrol == 7 && $idcategoria == 2) {
                        $existeSap = $user->consultaEmpleadoSap($num_documento);
                        if(isset($existeSap['value'][0]['EmployeeID'])){
                            $user->updateEmpleadoSap($existeSap['value'][0]['EmployeeID'],$data);
                        }else{
                            $user->insertaEmpleadoSap($data);
                        }
                    }
                }
                return 'USUARIO ACTUALIZADO';
            }else{
                return 'ERROR AL INSERTAR';
            }
        }
        if($query['cantidad'] == 1 && $query['iduser'] == $idempleado){
            if($user->updateUser($idempleado,$nombre,$apellido,$username,$fecha_nac,$direccion,$movil,$email_corporativo,$password_v,$tipo_documento,$num_documento,$idrol)){
                if ($idrol == 1 || $idrol == 4 || $idrol == 6 || $idrol == 7 || $idrol == 9 || $idrol == 11 || $idrol == 13 || $idrol == 16 || $idrol == 19){
                    $tecnico->updateTecnico($idtecnico,$idcategoria, $idSupervisor,$nombre,$apellido,$num_documento,$movil,$email_corporativo);
                    if($idrol == 7 && $idcategoria == 2) {
                        $existeSap = $user->consultaEmpleadoSap($num_documento);
                        if(isset($existeSap['value'][0]['EmployeeID'])){
                            $user->updateEmpleadoSap($existeSap['value'][0]['EmployeeID'],$data);
                        }else{
                            $user->insertaEmpleadoSap($data);
                        }
                    }
                }else{
                  $tecnico->desactivar($idtecnico);
                }
                return 'USUARIO ACTUALIZADO';
            }else{
                return 'ERROR AL INSERTAR';
            }
        }else{
            return 'NOMBRE DE USUARIO EXISTENTE';
        }
    }
}
function validarExisteNumDocumento(){
    global $user, $tecnico;
    $num_documento = $_POST["num_documento"];

    $rspta = $user->validarExisteNumDocumento($num_documento);
    $rsptaTecnico = $tecnico->validarExisteNumDocumento($num_documento);
	$data = Array();
    $data['datos'] = $rspta;  
    $data['datosTecnico'] = $rsptaTecnico;
    if($rspta['cantidad'] >0){
        $user_data = $user->dataEmployees($num_documento); 
        if($rsptaTecnico['condicion'] == 0 && $rsptaTecnico['condicion'] != null){
            $dataTec = $tecnico->dataTec($num_documento);
            $data['tecData'] = $dataTec;
        }
        $data['userData'] = $user_data;
        //$data['employees_table'] = $employees_data_exist;
    }
	echo json_encode($data);
}
function mostrar(){
    global $user, $tecnico;
    $iduser = $_POST["iduser"];
    $user->user($iduser, false, false, false, false, '1, 0');
    $tecnico->tecnico(false, false, false, false, false,$user->get_num_documento(), false, false, '1, 0');
    $data = array(
        "iduser"=>$user->get_iduser(),                       
        "username"=>$user->get_username(),
        "nombre"=>$user->get_nombre(),
        "apellido"=>$user->get_apellido(),
        "password"=>$user->get_password(),
        "tipo_documento"=>$user->get_tipo_document(),
        "num_documento"=>$user->get_num_documento(),
        "fecha_nac"=>$user->get_fec_nac(),
        "direccion"=>$user->get_direccion(),
        "movil"=>$user->get_telefono(),
        "email_corporativo"=>$user->get_email(),
        "idrol"=>$user->get_idrole(),
        "idtecnico"=>$tecnico->getIdtecnico(),
        "idsupervisor"=>$tecnico->getIdsupervisor(),
        "idcategoria"=>$tecnico->getIdcargotec()
    );
    return json_encode($data);
    
}
function desactivar(){
    global $user, $tecnico;
    $iduser = $_POST["iduser"];
    if($iduser != $_SESSION['iduser'] ) {
        $resp = $user->list($iduser,false, false, false, false, '1, 0');
        $data = Array();
        while ($row = db_fetch($resp)){
          $data= array(
            "num_documento"=>$row['num_documento']
          );
        }
        $rut = $data['num_documento'];
        //return 'pase';
        $respTec = $tecnico->dataTecActivo($rut);
        //return json_encode($respTec['cantidad']);
        if($user->desactivar($iduser)){
            if($respTec['cantidad'] >0){
               $tecnico->desactivar($respTec['idtecnico']);
            }
            return "Empleado inhabilitado";
        }else{
            return "Empleado no se pudo inhabilitar";
        }
    }else{
        return 'No puede deshabilitar el usuario con el que inicio sesión';
    }
    
}

function getRole() {
  global $role;
  $query = $role->list();
  echo '<option value="" selected disabled>SELECCIONE ROL</option>';
  while($row = db_fetch($query)){
    echo '<option value='.$row['idrole'].'>'.$row['nombre'].'</option>';
  }
}

function getCategoTec(){
	global $cargotec;
	$query = $cargotec->list();
	echo '<option value="" selected disabled>SELECCIONE CARGO</option>';
    while($row = db_fetch($query)){
        if($row['nombre'] != 'PRUEBA'){
          echo '<option value='.$row['idcargotec'].'>'.$row['nombre'].'</option>';
        }
    }
}

if (isset($_GET['exe']) AND $_GET['exe'] != '') {
	echo $_GET['exe']();
}
?>