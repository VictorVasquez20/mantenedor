<?php
session_start();
require_once "../config/conexion.php";

$contacto = new Contacto();
$edificio = new Edificio();
function getListContact(){
  global $contacto;
  $query = $contacto->listEdificio();
   $data = Array();
	while ($row = db_fetch($query)){
		$data['aaData'][] = array(
			"0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$row['idcontacto'].')" title="editar empleado"><i class="fa fa-pencil"></i></button>'.
                 '<button class="btn btn-danger btn-xs" onclick="desactivar('.$row['idcontacto'].')" title="desactivar empleado"><i class="fa fa-close"></i></button>',                       
			"1"=>$row['nombre'],
			"2"=>$row['cargo'],
            "3"=>$row['email'],
            "4"=>$row['nombreEdificio'],
            "5"=>$row['telefonomovil'],
            "6"=>$row['telefonoresi']
            
	);
	}
    echo json_encode($data);
}

function buscador(){
    global $edificio;
    $buscar = $_POST['buscar'];
    $query = $edificio->listLike($buscar);
    $data = Array();
    while($row = db_fetch($query)){
     $data[] = array(
        "idedificio" => $row['idedificio'],
        "nombre" => $row['nombre']
      );
        //echo '<div class="col-md-12 col-sm-12 col-xs-12"><input type="checkbox" class="js-switch" id='.$row['idedificio'].' name="ckpass" /> '.$row['nombre'].'</div>';
      }
      return json_encode($data);
}
function guardar(){
    global $contacto;
    $idBuscar = $_POST['idBuscar'];
    $contactosSys = $_POST['contacto'];
    $array_num = count($idBuscar);
    $array_num_contact = count($contactosSys);
    $bandera = false;
    $data = Array();
    for ($ia=0; $ia < $array_num_contact; $ia++) { 
      $nombre = $contactosSys[$ia]['nombre'];
      $cargo = $contactosSys[$ia]['cargo'];
      $email = $contactosSys[$ia]['email'];
      $telMovil = $contactosSys[$ia]['telMovil'];
      $telFijo = $contactosSys[$ia]['telFijo'];
        for ($i = 0; $i < $array_num; ++$i){
          $rsp = $contacto->validarCorreo($email,$idBuscar[$i]);
          if($rsp['cantidad'] == 0){  
            if($contacto->insertMany($nombre, $cargo, $email, $telMovil, $telFijo, $idBuscar[$i])){
              $data[] = array("idEdificiosGuardados" => $idBuscar[$i]);
              $bandera = true;
            }else{
              $bandera = false;
            }
          }else {
            $data[] = array("idEdificio" => $idBuscar[$i]);
            //array_push($data, $idBuscar[$i]);
          }
        }
      
    }
    
    if($bandera){
      $data[] =array("mensaje" => 'CONTACTO REGISTRADO');
      return json_encode($data);
    }else{
      $data[] =array("mensaje" => 'ERROR AL INSERTAR');
      return json_encode($data);
      //return 'ERROR AL INSERTAR';
    }
}
function mostrar(){
  global $contacto, $edificio;
  $id = $_POST['id'];
  $contacto->contactos($id);
  
  $data = array(
    "idcontacto" => $contacto->get_idcontacto(),
    "nombre" => $contacto->get_nombre(),
    "cargo" => $contacto->get_cargo(),
    "email" => $contacto->get_email(),
    "telefonomovil" => $contacto->get_telefonomovil(),
    "telefonoresi" => $contacto->get_telefonoresi(),
    "idedificio" => $contacto->get_idedificio()
  );
  return json_encode($data);
}

function deleteContacto(){
  global $contacto;
  $idcontacto = $_POST['idcontacto'];
  if($contacto->deleteContacto($idcontacto)){
    return 'El CONTACTO FUE ELIMINADO';
  }else{
    return 'ERROR AL INSERTAR';
  }
}

function editar(){
  global $contacto;
  $idEdit = $_POST['idEdit'];
  $nombreEdit = $_POST['nombreEdit'];
  $cargoEdit = $_POST['cargoEdit'];
  $emailEdit = $_POST['emailEdit'];
  $telMovilEdit = $_POST['telMovilEdit'];
  $telFijoEdit = $_POST['telFijoEdit'];
  if($contacto->update($idEdit ,$nombreEdit ,$cargoEdit ,$emailEdit ,$telMovilEdit ,$telFijoEdit)){
    return 'El CONTACTO FUE EDITADO';
  }else{
    return 'ERROR AL INSERTAR';
  }
}

if (isset($_GET['exe']) AND $_GET['exe'] != '') {
	echo $_GET['exe']();
}

