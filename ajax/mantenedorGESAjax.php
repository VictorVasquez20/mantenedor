<?php

session_start();
require_once "../config/conexion.php";
$tecnico = new Tecnico();
$servicio = new Servicio();
$ascensor = new Ascensor();
$edificio = new Edificio();
$tservicio = new Tservicio();
$testado = new Testado();
function getListGuia() {
	global $tecnico, $servicio, $ascensor, $edificio;
	$fechaDesde = $_POST["fechaDesde"];
    $fechaHasta = $_POST["fechaHasta"];
    $guia = $_POST["guia"];
	$query = $servicio->listDataTable($fechaDesde, $fechaHasta,$guia);
	
    $data = Array();
	while ($row = db_fetch($query)){
		$data['aaData'][] = array(
			"0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$row['idservicio'].')" title="editar usuario"><i class="fa fa-pencil"></i></button>',                       
			"1"=>$row['idservicio'],
			"2"=>$row['nombreEdi'],
			"3"=>$row['codigo'],
			"4"=>$row['nombreTec'].' '.$row['apellidoTec'],
	    );
	}
    echo json_encode($data);
}

function mostrar(){
  global $servicio; 
  $id = $_POST["id"];
  $query = $servicio->list($id);
  while ($row = db_fetch($query)){
	$data = array(
		"idservicio"=>$row['idservicio'],  
		"codigoFm"=>$row['idascensor'],
		"tipoServicio"=>$row['idtservicio'],
		"estadoInicio"=>$row['estadoini'],
		"estadoFin"=>$row['estadofin'],
		"obsInicio"=>$row['observacionini'],
		"obsFin"=>$row['observacionfin']
	  );
  }
  
  return json_encode($data);

}

function getAscensor(){
  global $ascensor;
  $query = $ascensor->list();
  echo '<option value="" selected disabled>SELECCIONE CODIGO FM</option>';
  while($row = db_fetch($query)){
	echo '<option value='.$row['idascensor'].'>'.$row['codigo'].'</option>';
  }
}

function getTServicio(){
	global $tservicio;
	$query = $tservicio->list();
    echo '<option value="" selected disabled>SELECCIONE TIPO SERVICIO</option>';
    while($row = db_fetch($query)){
	  echo '<option value='.$row['idtservicio'].'>'.$row['nombre'].'</option>';
    }
}

function getEstado(){
	global $testado;
	$query = $testado->list();
    echo '<option value="" selected disabled>SELECCIONE ESTADO</option>';
    while($row = db_fetch($query)){
	  echo '<option value='.$row['id'].'>'.$row['nombre'].'</option>';
    }
}

function editar(){
  global $servicio; 
  $idservicio = $_POST['idservicio'];
  $codigoFm = $_POST['codigoFm'];
  $tipoServicio = $_POST['tipoServicio'];
  $estadoInicio = $_POST['estadoInicio'];
  $estadoFin = $_POST['estadoFin'];
  $obsInicio = strtoupper($_POST['obsInicio']);
  $obsFin = strtoupper($_POST['obsFin']);
    if($servicio->update($idservicio, $codigoFm, $tipoServicio, $estadoInicio, $estadoFin, $obsInicio, $obsFin)){
	    return 'DATOS DEL SERVICIO ACTUALIZADO';
    }else{
 	    return 'ERROR AL INSERTAR';
    }

}

if (isset($_GET['exe']) AND $_GET['exe'] != '') {
	echo $_GET['exe']();
}