<?php 

require_once "global.php";
require_once "Autoload.php";
$Config = new Config();

if(!function_exists('ejecutarConsulta')){
  function db_query($sql){
  	try{
  	  global $Config;
  	  $conexion = $Config->conection();
  	  $resp = $conexion->query($sql);
  	  if (!$resp) {
  	  	throw new Exception('Algo fallo contactece con soporte',3000);
  	   }
  	  return $resp;
  	}catch(Exception $e){
  	  $mensaje = str_replace("'",'',$conexion->error);
  	  $mensajeR["cod"] = 220;
  	  $mensajeR["mensaje"] = $conexion->error;
  	  echo json_encode($mensajeR);
  	  die();
  	}finally{
  	  $conexion->close();
    } 
  }

  function db_fetch($query)
  {
  	try{
  		global $Config;
  		$conexion = $Config->conection();
  		$query = $query->fetch_assoc();
  		if ($query !== null && !$query) {
  			throw new Exception('Algo fallo contactece con soporte',3000);
  		}
  		return $query;
  	}catch(Exception $e){
  		$mensaje = str_replace("'",'',$conexion->error);
  		$mensajeR["cod"] = 220;
  		$mensajeR["mensaje"] = $e->getMessage();
  		echo json_encode($mensajeR);
  		die();
  	}finally{
  		$conexion->close();
  	}
  }

  function ejecutarConsulta($sql){
  	try{
  		global $Config;
  		$conexion = $Config->conection();
  		$query = $conexion->query($sql);
  		if (!$query) {
  			throw new Exception('Algo fallo contactece con soporte',3000);
  		}
  		return $query;
  	}catch(Exception $e){
  		$mensaje = str_replace("'",'',$conexion->error);
  		$mensajeR["cod"] = 220;
  		$mensajeR["mensaje"] = $conexion->error;
  		echo json_encode($mensajeR);
  		die();
  	}finally{
  		$conexion->close();
  	}
  }  
  function ejecutarConsultaSimpleFila($sql){
  	try{
  		global $Config;
  		$conexion = $Config->conection();
  		$query = $conexion->query($sql);
  		$row = $query->fetch_assoc();
  		if (!$row) {
  			throw new Exception('Algo fallo contactece con soporte',3000);
  		}
  		return $row;
  	}catch(Exception $e){
  		$mensaje = str_replace("'",'',$conexion->error);
  		$mensajeR["cod"] = 220;
  		$mensajeR["mensaje"] = $e->getMessage();
  		echo json_encode($mensajeR);
  		die();
  	}finally{
  		$conexion->close();
  	}
  }  
  function ejecutarConsulta_retornarID($sql){
  	try{
  		global $Config;
  		$query = $conexion->query($sql);
  		if (!$query) {
  			throw new Exception('Algo fallo contactece con soporte',3000);
  		}
  		$id = $conexion->insert_id;
  		return $id;
  	}catch(Exception $e){
  		$mensaje = str_replace("'",'',$conexion->error);
  		$mensajeR["cod"] = 220;
  		$mensajeR["mensaje"] = $e->getMessage();
  		echo json_encode($mensajeR);
  		die();
  	}finally{
  		$conexion->close();
  	}
  }  
  function limpiarCadena($str){
  	try{
  		global $Config;
  		$str = mysqli_real_escape_string($conexion, trim($str));
  		if (!$str) {
  			throw new Exception('Algo fallo contactece con soporte',3000);
  		}
  		return htmlspecialchars($str);
  	}catch(Exception $e){
  		$mensaje = str_replace("'",'',$conexion->error);
  		$mensajeR["cod"] = 220;
  		$mensajeR["mensaje"] = $e->getMessage();
  		echo json_encode($mensajeR);
  		die();
  	}finally{
  		$conexion->close();
  	}
  }



}


?>